EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L arduradio:FDC6329L U?
U 1 1 6021AEDB
P 5610 3680
F 0 "U?" H 5960 4155 50  0000 C CNN
F 1 "FDC6329L" H 5960 4064 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 6060 3580 50  0001 C CNN
F 3 "" H 5610 3680 50  0001 C CNN
	1    5610 3680
	1    0    0    -1  
$EndComp
$EndSCHEMATC
