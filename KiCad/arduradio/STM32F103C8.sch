EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 6004CF5A
P 600 7220
AR Path="/5FDB8EF8/6004CF5A" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/6004CF5A" Ref="#PWR021"  Part="1" 
AR Path="/6004CF5A" Ref="#PWR?"  Part="1" 
F 0 "#PWR021" H 600 6970 50  0001 C CNN
F 1 "GND" H 605 7047 50  0000 C CNN
F 2 "" H 600 7220 50  0001 C CNN
F 3 "" H 600 7220 50  0001 C CNN
	1    600  7220
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  7220 600  7150
Wire Wire Line
	600  6710 600  6650
Text Notes 530  6170 0    101  ~ 20
Capacitors
$Comp
L arduradio-rescue:C-Device C?
U 1 1 6004CF69
P 1520 7000
AR Path="/5FDB8EF8/6004CF69" Ref="C?"  Part="1" 
AR Path="/6003A36D/6004CF69" Ref="C17"  Part="1" 
F 0 "C17" H 1635 7046 50  0000 L CNN
F 1 "100uF" H 1635 6955 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 1558 6850 50  0001 C CNN
F 3 "~" H 1520 7000 50  0001 C CNN
	1    1520 7000
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 6004CF6F
P 2020 7000
AR Path="/5FDB8EF8/6004CF6F" Ref="C?"  Part="1" 
AR Path="/6003A36D/6004CF6F" Ref="C19"  Part="1" 
F 0 "C19" H 2135 7046 50  0000 L CNN
F 1 "100uF" H 2135 6955 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 2058 6850 50  0001 C CNN
F 3 "~" H 2020 7000 50  0001 C CNN
	1    2020 7000
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 6004CF75
P 2460 7000
AR Path="/5FDB8EF8/6004CF75" Ref="C?"  Part="1" 
AR Path="/6003A36D/6004CF75" Ref="C21"  Part="1" 
F 0 "C21" H 2575 7046 50  0000 L CNN
F 1 "100nF" H 2575 6955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2498 6850 50  0001 C CNN
F 3 "~" H 2460 7000 50  0001 C CNN
	1    2460 7000
	1    0    0    -1  
$EndComp
Connection ~ 1520 7150
Wire Wire Line
	1520 7150 2020 7150
Connection ~ 2020 7150
Wire Wire Line
	2020 7150 2460 7150
Wire Wire Line
	600  6710 1030 6710
Wire Wire Line
	2460 6850 2460 6710
Wire Wire Line
	2020 6850 2020 6710
Connection ~ 2020 6710
Wire Wire Line
	2020 6710 2460 6710
Wire Wire Line
	1520 6850 1520 6710
Connection ~ 1520 6710
Wire Wire Line
	1520 6710 2020 6710
Connection ~ 1030 6710
Wire Wire Line
	1030 6710 1520 6710
Text Notes 1010 3110 0    101  ~ 20
Rotary Encoder
Wire Wire Line
	1680 4900 1710 4900
Wire Wire Line
	1680 5100 1720 5100
Wire Wire Line
	2330 4900 2360 4900
Wire Wire Line
	2410 5100 2410 5400
Wire Wire Line
	2330 5100 2410 5100
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 6004CF94
P 2410 5540
AR Path="/5FDB8EF8/6004CF94" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/6004CF94" Ref="#PWR028"  Part="1" 
AR Path="/6004CF94" Ref="#PWR?"  Part="1" 
F 0 "#PWR028" H 2410 5290 50  0001 C CNN
F 1 "GND" H 2415 5367 50  0000 C CNN
F 2 "" H 2410 5540 50  0001 C CNN
F 3 "" H 2410 5540 50  0001 C CNN
	1    2410 5540
	1    0    0    -1  
$EndComp
Text Label 2440 4900 0    50   ~ 0
Encoder_Button
Wire Wire Line
	1090 5000 1730 5000
Wire Wire Line
	1090 5080 1090 5000
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 6004CF9D
P 1090 5080
AR Path="/5FDB8EF8/6004CF9D" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/6004CF9D" Ref="#PWR023"  Part="1" 
AR Path="/6004CF9D" Ref="#PWR?"  Part="1" 
F 0 "#PWR023" H 1090 4830 50  0001 C CNN
F 1 "GND" H 1095 4907 50  0000 C CNN
F 2 "" H 1090 5080 50  0001 C CNN
F 3 "" H 1090 5080 50  0001 C CNN
	1    1090 5080
	1    0    0    -1  
$EndComp
Text Label 1680 5100 2    50   ~ 0
Encoder_B
Text Label 1680 4900 2    50   ~ 0
Encoder_A
Wire Wire Line
	1860 3790 2030 3790
Wire Wire Line
	1860 3860 1860 3790
Wire Wire Line
	2030 3590 1850 3590
Text Label 1860 3690 2    50   ~ 0
Encoder_Button
Text Label 1850 3590 2    50   ~ 0
Encoder_B
Text Label 1860 3490 2    50   ~ 0
Encoder_A
$Comp
L arduradio-rescue:Conn_01x04-Connector_Generic J?
U 1 1 6004CFAB
P 2230 3590
AR Path="/5FDB8EF8/6004CFAB" Ref="J?"  Part="1" 
AR Path="/6003A36D/6004CFAB" Ref="J9"  Part="1" 
F 0 "J9" H 2310 3582 50  0000 L CNN
F 1 "Conn_01x04" H 2310 3491 50  0001 L CNN
F 2 "Connector_JST:JST_PH_B4B-PH-K_1x04_P2.00mm_Vertical" H 2230 3590 50  0001 C CNN
F 3 "~" H 2230 3590 50  0001 C CNN
	1    2230 3590
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 6004CFB1
P 1860 3860
AR Path="/5FDB8EF8/6004CFB1" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/6004CFB1" Ref="#PWR024"  Part="1" 
AR Path="/6004CFB1" Ref="#PWR?"  Part="1" 
F 0 "#PWR024" H 1860 3610 50  0001 C CNN
F 1 "GND" H 1865 3687 50  0000 C CNN
F 2 "" H 1860 3860 50  0001 C CNN
F 3 "" H 1860 3860 50  0001 C CNN
	1    1860 3860
	1    0    0    -1  
$EndComp
Wire Wire Line
	1860 3690 2030 3690
Wire Wire Line
	1860 3490 2030 3490
$Comp
L arduradio-rescue:Rotary_Encoder_Switch-Device SW?
U 1 1 6004CFB9
P 2030 5000
AR Path="/5FDB8EF8/6004CFB9" Ref="SW?"  Part="1" 
AR Path="/6003A36D/6004CFB9" Ref="SW1"  Part="1" 
F 0 "SW1" H 2030 5270 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 2030 5276 50  0001 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC11E-Switch_Vertical_H20mm" H 1880 5160 50  0001 C CNN
F 3 "~" H 2030 5260 50  0001 C CNN
	1    2030 5000
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:R_POT-Device RV?
U 1 1 60057B8B
P 2190 2220
AR Path="/5FDB8EF8/60057B8B" Ref="RV?"  Part="1" 
AR Path="/6003A36D/60057B8B" Ref="RV1"  Part="1" 
F 0 "RV1" H 2090 2330 50  0000 R CNN
F 1 "50k" H 2080 2160 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3386P_Vertical" H 2190 2220 50  0001 C CNN
F 3 "~" H 2190 2220 50  0001 C CNN
	1    2190 2220
	-1   0    0    -1  
$EndComp
Text Label 1950 2170 2    50   ~ 0
Poti_Input
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60057B92
P 2190 1810
AR Path="/5FDB8EF8/60057B92" Ref="R?"  Part="1" 
AR Path="/6003A36D/60057B92" Ref="R9"  Part="1" 
F 0 "R9" H 2050 1730 50  0000 C CNN
F 1 "100k" H 2030 1900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2120 1810 50  0001 C CNN
F 3 "~" H 2190 1810 50  0001 C CNN
	1    2190 1810
	-1   0    0    1   
$EndComp
Wire Wire Line
	2190 2070 2190 2010
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 60057B9F
P 2190 2440
AR Path="/5FDB8EF8/60057B9F" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/60057B9F" Ref="#PWR026"  Part="1" 
AR Path="/60057B9F" Ref="#PWR?"  Part="1" 
F 0 "#PWR026" H 2190 2190 50  0001 C CNN
F 1 "GND" H 2195 2267 50  0000 C CNN
F 2 "" H 2190 2440 50  0001 C CNN
F 3 "" H 2190 2440 50  0001 C CNN
	1    2190 2440
	1    0    0    -1  
$EndComp
Text Notes 1290 1120 0    101  ~ 20
Anlaog Input
Wire Wire Line
	2190 1660 2190 1580
$Comp
L arduradio-rescue:Conn_01x03-Connector_Generic J?
U 1 1 60057BA7
P 1230 2220
AR Path="/5FDB8EF8/60057BA7" Ref="J?"  Part="1" 
AR Path="/6003A36D/60057BA7" Ref="J8"  Part="1" 
AR Path="/60057BA7" Ref="J?"  Part="1" 
F 0 "J8" H 1148 2445 50  0000 C CNN
F 1 "Conn_01x03" H 1148 2446 50  0001 C CNN
F 2 "Connector_JST:JST_PH_B3B-PH-K_1x03_P2.00mm_Vertical" H 1230 2220 50  0001 C CNN
F 3 "~" H 1230 2220 50  0001 C CNN
	1    1230 2220
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1430 2120 1500 2120
Wire Wire Line
	1500 2120 1500 2010
Wire Wire Line
	1500 2010 2190 2010
Connection ~ 2190 2010
Wire Wire Line
	2190 2010 2190 1960
Wire Wire Line
	2040 2220 1950 2220
Wire Wire Line
	2190 2400 1500 2400
Wire Wire Line
	2190 2370 2190 2400
Connection ~ 2190 2400
Wire Wire Line
	2190 2400 2190 2440
Wire Wire Line
	1500 2400 1500 2320
Wire Wire Line
	1500 2320 1430 2320
Wire Wire Line
	1950 2170 1950 2220
Connection ~ 1950 2220
Wire Wire Line
	1950 2220 1430 2220
$Comp
L arduradio-rescue:Crystal-Device Y2
U 1 1 6005FDBB
P 7920 3140
F 0 "Y2" V 7874 3271 50  0000 L CNN
F 1 "8MHz" V 7965 3271 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_HC49-SD" H 7920 3140 50  0001 C CNN
F 3 "~" H 7920 3140 50  0001 C CNN
	1    7920 3140
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 60060EE4
P 7320 2830
AR Path="/5FDB8EF8/60060EE4" Ref="C?"  Part="1" 
AR Path="/6003A36D/60060EE4" Ref="C31"  Part="1" 
F 0 "C31" V 7450 2880 50  0000 L CNN
F 1 "20pF" V 7450 2590 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7358 2680 50  0001 C CNN
F 3 "~" H 7320 2830 50  0001 C CNN
	1    7320 2830
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 600621AD
P 7320 3440
AR Path="/5FDB8EF8/600621AD" Ref="C?"  Part="1" 
AR Path="/6003A36D/600621AD" Ref="C32"  Part="1" 
F 0 "C32" V 7170 3530 50  0000 L CNN
F 1 "20pF" V 7170 3220 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7358 3290 50  0001 C CNN
F 3 "~" H 7320 3440 50  0001 C CNN
	1    7320 3440
	0    1    1    0   
$EndComp
Wire Wire Line
	7470 2830 7920 2830
Wire Wire Line
	7920 2830 7920 2990
Wire Wire Line
	7470 3440 7920 3440
Wire Wire Line
	7920 3440 7920 3290
Wire Wire Line
	7170 2830 6890 2830
Wire Wire Line
	6890 3440 7170 3440
Wire Wire Line
	6890 2830 6890 3440
Connection ~ 7920 3440
Wire Wire Line
	8550 2830 8550 3340
Connection ~ 7920 2830
$Comp
L arduradio-rescue:Crystal-Device Y1
U 1 1 600997A5
P 7670 4040
F 0 "Y1" V 7716 4171 50  0000 L CNN
F 1 "32k768" V 7625 4171 50  0000 L CNN
F 2 "Crystal:Crystal_DS26_D2.0mm_L6.0mm_Vertical" H 7670 4040 50  0001 C CNN
F 3 "~" H 7670 4040 50  0001 C CNN
	1    7670 4040
	0    1    -1   0   
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 600997AB
P 7320 4350
AR Path="/5FDB8EF8/600997AB" Ref="C?"  Part="1" 
AR Path="/6003A36D/600997AB" Ref="C34"  Part="1" 
F 0 "C34" V 7450 4400 50  0000 L CNN
F 1 "20pF" V 7450 4110 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7358 4200 50  0001 C CNN
F 3 "~" H 7320 4350 50  0001 C CNN
	1    7320 4350
	0    1    -1   0   
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 600997B1
P 7320 3740
AR Path="/5FDB8EF8/600997B1" Ref="C?"  Part="1" 
AR Path="/6003A36D/600997B1" Ref="C33"  Part="1" 
F 0 "C33" V 7170 3800 50  0000 L CNN
F 1 "20pF" V 7180 3510 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7358 3590 50  0001 C CNN
F 3 "~" H 7320 3740 50  0001 C CNN
	1    7320 3740
	0    1    -1   0   
$EndComp
Wire Wire Line
	7470 4350 7670 4350
Wire Wire Line
	7670 4350 7670 4190
Wire Wire Line
	7470 3740 7670 3740
Wire Wire Line
	7670 3740 7670 3890
Wire Wire Line
	7170 4350 6890 4350
Wire Wire Line
	6890 3740 7170 3740
Wire Wire Line
	6890 4350 6890 3740
Wire Wire Line
	8120 4350 8120 3840
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 600A775F
P 6890 4470
AR Path="/5FDB8EF8/600A775F" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/600A775F" Ref="#PWR046"  Part="1" 
AR Path="/600A775F" Ref="#PWR?"  Part="1" 
F 0 "#PWR046" H 6890 4220 50  0001 C CNN
F 1 "GND" H 6895 4297 50  0000 C CNN
F 2 "" H 6890 4470 50  0001 C CNN
F 3 "" H 6890 4470 50  0001 C CNN
	1    6890 4470
	1    0    0    -1  
$EndComp
Wire Wire Line
	6890 4470 6890 4350
Connection ~ 6890 4350
Wire Wire Line
	6890 3740 6890 3440
Wire Wire Line
	6890 3440 6900 3440
Connection ~ 6890 3740
Connection ~ 6890 3440
Wire Wire Line
	8550 3340 9070 3340
Text Label 9010 2940 2    50   ~ 0
RESET
Text Label 9000 3140 2    50   ~ 0
BOOT0
Wire Wire Line
	9010 2940 9070 2940
Wire Wire Line
	9000 3140 9070 3140
$Comp
L arduradio-rescue:STM32F103C8Tx-MCU_ST_STM32F1 U4
U 1 1 6005CCDF
P 9770 4240
F 0 "U4" H 10210 5740 50  0000 C CNN
F 1 "STM32F103C8Tx" H 9180 5750 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 9170 2840 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00161566.pdf" H 9770 4240 50  0001 C CNN
	1    9770 4240
	1    0    0    -1  
$EndComp
Wire Wire Line
	9670 2740 9670 2620
Wire Wire Line
	9670 2620 9770 2620
Wire Wire Line
	9770 2620 9770 2740
Wire Wire Line
	9770 2620 9870 2620
Wire Wire Line
	9870 2620 9870 2740
Connection ~ 9770 2620
Wire Wire Line
	9870 2620 9970 2620
Wire Wire Line
	9970 2620 9970 2740
Connection ~ 9870 2620
Wire Wire Line
	9670 2620 9670 2470
Connection ~ 9670 2620
Wire Wire Line
	9570 5740 9570 5870
Wire Wire Line
	9570 5870 9670 5870
Wire Wire Line
	9670 5870 9670 5740
Wire Wire Line
	9670 5870 9770 5870
Wire Wire Line
	9770 5870 9770 5740
Connection ~ 9670 5870
Wire Wire Line
	9770 5870 9870 5870
Wire Wire Line
	9870 5870 9870 5740
Connection ~ 9770 5870
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 600C7072
P 9570 6310
AR Path="/5FDB8EF8/600C7072" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/600C7072" Ref="#PWR054"  Part="1" 
AR Path="/600C7072" Ref="#PWR?"  Part="1" 
F 0 "#PWR054" H 9570 6060 50  0001 C CNN
F 1 "GND" H 9575 6137 50  0000 C CNN
F 2 "" H 9570 6310 50  0001 C CNN
F 3 "" H 9570 6310 50  0001 C CNN
	1    9570 6310
	1    0    0    -1  
$EndComp
Connection ~ 9570 5870
Text Notes 7920 2420 0    101  ~ 20
Microcontroller
Wire Wire Line
	6560 2590 6660 2590
Text Notes 5260 2090 0    101  ~ 20
Button Input
Text Label 4700 2590 2    50   ~ 0
Mode_Switch
Wire Wire Line
	1030 7150 1520 7150
Connection ~ 1030 7150
Wire Wire Line
	600  7150 1030 7150
Wire Wire Line
	1030 6850 1030 6710
$Comp
L arduradio-rescue:C-Device C?
U 1 1 6004CF63
P 1030 7000
AR Path="/5FDB8EF8/6004CF63" Ref="C?"  Part="1" 
AR Path="/6003A36D/6004CF63" Ref="C16"  Part="1" 
F 0 "C16" H 1145 7046 50  0000 L CNN
F 1 "100nF" H 1145 6955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1068 6850 50  0001 C CNN
F 3 "~" H 1030 7000 50  0001 C CNN
	1    1030 7000
	1    0    0    -1  
$EndComp
Text Label 10870 4440 0    50   ~ 0
TFT_CS
Text Label 10870 4540 0    50   ~ 0
CLK
Text Label 10870 4640 0    50   ~ 0
MISO
Text Label 10870 4740 0    50   ~ 0
MOSI
Text Label 10460 4040 0    50   ~ 0
Poti_Input
Wire Wire Line
	10370 4040 10460 4040
Text Label 8520 5240 2    50   ~ 0
Encoder_Button
Text Label 8520 5140 2    50   ~ 0
Encoder_B
Text Label 8520 5040 2    50   ~ 0
Encoder_A
Wire Wire Line
	8980 5040 9070 5040
Wire Wire Line
	8980 5140 9070 5140
Wire Wire Line
	8980 5240 9070 5240
Text HLabel 7480 4740 0    50   BiDi ~ 0
SDIO
Text HLabel 7480 4640 0    50   Output ~ 0
SCLK
Text HLabel 8560 4840 0    50   Output ~ 0
SI473X_RESET
$Comp
L arduradio-rescue:R-Device R?
U 1 1 604877E9
P 8840 4840
AR Path="/5FDB8EF8/604877E9" Ref="R?"  Part="1" 
AR Path="/6003A36D/604877E9" Ref="R33"  Part="1" 
F 0 "R33" V 8790 4670 50  0000 C CNN
F 1 "1k" V 8790 5010 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8770 4840 50  0001 C CNN
F 3 "~" H 8840 4840 50  0001 C CNN
	1    8840 4840
	0    1    1    0   
$EndComp
Wire Wire Line
	8990 4840 9070 4840
Wire Wire Line
	8560 4840 8690 4840
$Comp
L arduradio-rescue:R-Device R?
U 1 1 604980F8
P 7700 4980
AR Path="/5FDB8EF8/604980F8" Ref="R?"  Part="1" 
AR Path="/6003A36D/604980F8" Ref="R24"  Part="1" 
F 0 "R24" H 7570 4920 50  0000 C CNN
F 1 "3k3" H 7570 5010 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7630 4980 50  0001 C CNN
F 3 "~" H 7700 4980 50  0001 C CNN
	1    7700 4980
	-1   0    0    1   
$EndComp
$Comp
L arduradio-rescue:R-Device R?
U 1 1 6049B316
P 7590 4980
AR Path="/5FDB8EF8/6049B316" Ref="R?"  Part="1" 
AR Path="/6003A36D/6049B316" Ref="R23"  Part="1" 
F 0 "R23" H 7460 5040 50  0000 C CNN
F 1 "3k3" H 7460 4950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7520 4980 50  0001 C CNN
F 3 "~" H 7590 4980 50  0001 C CNN
	1    7590 4980
	1    0    0    -1  
$EndComp
Wire Wire Line
	7480 4640 7590 4640
Wire Wire Line
	7480 4740 7700 4740
Wire Wire Line
	7590 4830 7590 4640
Connection ~ 7590 4640
Wire Wire Line
	7590 4640 9070 4640
Wire Wire Line
	7700 4830 7700 4740
Connection ~ 7700 4740
Wire Wire Line
	7700 4740 9070 4740
Wire Wire Line
	7590 5290 7590 5200
Wire Wire Line
	7590 5200 7700 5200
Wire Wire Line
	7700 5200 7700 5130
Connection ~ 7590 5200
Wire Wire Line
	7590 5200 7590 5130
Wire Wire Line
	10370 5140 10470 5140
Wire Wire Line
	10370 5240 10470 5240
Text Label 9000 4240 2    50   ~ 0
BOOT1
Wire Wire Line
	9000 4240 9070 4240
Text HLabel 8560 4540 0    50   Input ~ 0
SI473X_INT
$Comp
L arduradio-rescue:R-Device R?
U 1 1 6053249A
P 8840 4540
AR Path="/5FDB8EF8/6053249A" Ref="R?"  Part="1" 
AR Path="/6003A36D/6053249A" Ref="R32"  Part="1" 
F 0 "R32" V 8790 4350 50  0000 C CNN
F 1 "1k" V 8790 4710 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8770 4540 50  0001 C CNN
F 3 "~" H 8840 4540 50  0001 C CNN
	1    8840 4540
	0    1    1    0   
$EndComp
Wire Wire Line
	8990 4540 9070 4540
Wire Wire Line
	8560 4540 8690 4540
$Comp
L arduradio-rescue:USB_B_Micro-Connector J?
U 1 1 605417F6
P 7700 1150
AR Path="/605417F6" Ref="J?"  Part="1" 
AR Path="/6003A36D/605417F6" Ref="J11"  Part="1" 
F 0 "J11" V 7711 1480 50  0000 L CNN
F 1 "USB_B_Micro" V 7802 1480 50  0001 L CNN
F 2 "Connector_USB:USB_Mini-B_Wuerth_65100516121_Horizontal" H 7850 1100 50  0001 C CNN
F 3 "~" H 7850 1100 50  0001 C CNN
	1    7700 1150
	0    1    1    0   
$EndComp
Wire Wire Line
	7300 1150 7210 1150
Wire Wire Line
	7210 1150 7210 1050
Wire Wire Line
	7210 1050 7300 1050
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 6054D71D
P 6930 1180
AR Path="/5FDB8EF8/6054D71D" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/6054D71D" Ref="#PWR047"  Part="1" 
AR Path="/6054D71D" Ref="#PWR?"  Part="1" 
F 0 "#PWR047" H 6930 930 50  0001 C CNN
F 1 "GND" H 6935 1007 50  0000 C CNN
F 2 "" H 6930 1180 50  0001 C CNN
F 3 "" H 6930 1180 50  0001 C CNN
	1    6930 1180
	1    0    0    -1  
$EndComp
Wire Wire Line
	6930 1180 6930 1050
Wire Wire Line
	6930 1050 7210 1050
Connection ~ 7210 1050
NoConn ~ 7500 1450
$Comp
L arduradio-rescue:+5V-power #PWR052
U 1 1 60561AF0
P 8750 1540
F 0 "#PWR052" H 8750 1390 50  0001 C CNN
F 1 "+5V" H 8765 1713 50  0000 C CNN
F 2 "" H 8750 1540 50  0001 C CNN
F 3 "" H 8750 1540 50  0001 C CNN
	1    8750 1540
	-1   0    0    1   
$EndComp
Wire Wire Line
	7900 1530 7900 1450
Text Label 7600 1560 3    50   ~ 0
USB-
Text Label 10870 5140 0    50   ~ 0
USB-
Text Label 10870 5240 0    50   ~ 0
USB+
Text Label 10870 5340 0    50   ~ 0
SWDIO
Text Label 10870 5440 0    50   ~ 0
SWCLK
Wire Wire Line
	10770 5140 10870 5140
Wire Wire Line
	10770 5240 10870 5240
Wire Wire Line
	10370 5340 10870 5340
Wire Wire Line
	10370 5440 10870 5440
Text Label 7700 1560 3    50   ~ 0
USB+
Wire Wire Line
	7600 1560 7600 1450
Wire Wire Line
	7700 1560 7700 1500
Wire Wire Line
	8520 5040 8680 5040
Wire Wire Line
	8680 5140 8520 5140
Wire Wire Line
	8520 5240 8680 5240
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60739E38
P 8830 5240
AR Path="/5FDB8EF8/60739E38" Ref="R?"  Part="1" 
AR Path="/6003A36D/60739E38" Ref="R28"  Part="1" 
F 0 "R28" V 8780 5040 50  0000 C CNN
F 1 "1k" V 8780 5420 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8760 5240 50  0001 C CNN
F 3 "~" H 8830 5240 50  0001 C CNN
	1    8830 5240
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60739BEA
P 8830 5140
AR Path="/5FDB8EF8/60739BEA" Ref="R?"  Part="1" 
AR Path="/6003A36D/60739BEA" Ref="R27"  Part="1" 
F 0 "R27" V 8780 4950 50  0000 C CNN
F 1 "1k" V 8780 5320 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8760 5140 50  0001 C CNN
F 3 "~" H 8830 5140 50  0001 C CNN
	1    8830 5140
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:R-Device R?
U 1 1 6073959C
P 8830 5040
AR Path="/5FDB8EF8/6073959C" Ref="R?"  Part="1" 
AR Path="/6003A36D/6073959C" Ref="R26"  Part="1" 
F 0 "R26" V 8780 4850 50  0000 C CNN
F 1 "1k" V 8780 5220 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8760 5040 50  0001 C CNN
F 3 "~" H 8830 5040 50  0001 C CNN
	1    8830 5040
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:R-Device R?
U 1 1 6077884D
P 8010 1980
AR Path="/5FDB8EF8/6077884D" Ref="R?"  Part="1" 
AR Path="/6003A36D/6077884D" Ref="R25"  Part="1" 
F 0 "R25" V 7960 1810 50  0000 C CNN
F 1 "1k5" V 7960 2160 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7940 1980 50  0001 C CNN
F 3 "~" H 8010 1980 50  0001 C CNN
	1    8010 1980
	0    1    1    0   
$EndComp
Wire Wire Line
	8160 1980 8400 1980
Wire Wire Line
	8400 1980 8400 1940
Wire Wire Line
	7860 1980 7770 1980
Wire Wire Line
	7770 1980 7770 1500
Wire Wire Line
	7770 1500 7700 1500
Connection ~ 7700 1500
Wire Wire Line
	7700 1500 7700 1450
Text Notes 7120 810  0    101  ~ 20
USB Interface
$Comp
L arduradio-rescue:Conn_01x04-Connector_Generic J?
U 1 1 6020C17B
P 10140 1100
AR Path="/5FDB8EF8/6020C17B" Ref="J?"  Part="1" 
AR Path="/6003A36D/6020C17B" Ref="J12"  Part="1" 
F 0 "J12" V 10140 730 50  0000 L CNN
F 1 "Conn_01x04" H 10220 1001 50  0001 L CNN
F 2 "Connector_JST:JST_PH_B4B-PH-K_1x04_P2.00mm_Vertical" H 10140 1100 50  0001 C CNN
F 3 "~" H 10140 1100 50  0001 C CNN
	1    10140 1100
	0    -1   -1   0   
$EndComp
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 6020DB43
P 10830 1410
AR Path="/5FDB8EF8/6020DB43" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/6020DB43" Ref="#PWR056"  Part="1" 
AR Path="/6020DB43" Ref="#PWR?"  Part="1" 
F 0 "#PWR056" H 10830 1160 50  0001 C CNN
F 1 "GND" H 10835 1237 50  0000 C CNN
F 2 "" H 10830 1410 50  0001 C CNN
F 3 "" H 10830 1410 50  0001 C CNN
	1    10830 1410
	1    0    0    -1  
$EndComp
Text Label 10140 1740 3    50   ~ 0
SWDIO
Text Label 10240 1740 3    50   ~ 0
SWCLK
$Comp
L arduradio-rescue:R-Device R?
U 1 1 602307D1
P 9410 1610
AR Path="/5FDB8EF8/602307D1" Ref="R?"  Part="1" 
AR Path="/6003A36D/602307D1" Ref="R34"  Part="1" 
F 0 "R34" H 9270 1570 50  0000 C CNN
F 1 "10k" H 9260 1670 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9340 1610 50  0001 C CNN
F 3 "~" H 9410 1610 50  0001 C CNN
	1    9410 1610
	1    0    0    -1  
$EndComp
Wire Wire Line
	10830 1410 10830 1300
Wire Wire Line
	10340 1300 10570 1300
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60251DD2
P 10570 1600
AR Path="/5FDB8EF8/60251DD2" Ref="R?"  Part="1" 
AR Path="/6003A36D/60251DD2" Ref="R35"  Part="1" 
F 0 "R35" H 10450 1560 50  0000 C CNN
F 1 "10k" H 10440 1660 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10500 1600 50  0001 C CNN
F 3 "~" H 10570 1600 50  0001 C CNN
	1    10570 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9410 1460 9410 1420
Wire Wire Line
	10040 1830 10040 1540
Wire Wire Line
	10040 1540 10140 1540
Wire Wire Line
	10140 1540 10140 1300
Wire Wire Line
	10240 1300 10240 1540
Wire Wire Line
	10140 1740 10140 1540
Connection ~ 10140 1540
Wire Wire Line
	10570 1450 10570 1300
Connection ~ 10570 1300
Wire Wire Line
	10570 1300 10830 1300
Wire Wire Line
	10570 1750 10570 1810
Wire Wire Line
	10570 1810 10330 1810
Wire Wire Line
	10330 1810 10330 1540
Wire Wire Line
	10330 1540 10240 1540
Connection ~ 10240 1540
Wire Wire Line
	10240 1540 10240 1740
Text Notes 9660 900  0    101  ~ 20
SWD Interface
Text Label 4310 1300 2    50   ~ 0
BOOT0
Text Label 6010 1300 0    50   ~ 0
BOOT1
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 602E1225
P 5170 1610
AR Path="/5FDB8EF8/602E1225" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/602E1225" Ref="#PWR034"  Part="1" 
AR Path="/602E1225" Ref="#PWR?"  Part="1" 
F 0 "#PWR034" H 5170 1360 50  0001 C CNN
F 1 "GND" H 5175 1437 50  0000 C CNN
F 2 "" H 5170 1610 50  0001 C CNN
F 3 "" H 5170 1610 50  0001 C CNN
	1    5170 1610
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:R-Device R?
U 1 1 602FFE2D
P 4580 1300
AR Path="/5FDB8EF8/602FFE2D" Ref="R?"  Part="1" 
AR Path="/6003A36D/602FFE2D" Ref="R20"  Part="1" 
F 0 "R20" V 4490 1300 50  0000 C CNN
F 1 "100k" V 4680 1300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4510 1300 50  0001 C CNN
F 3 "~" H 4580 1300 50  0001 C CNN
	1    4580 1300
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60300FB9
P 5700 1300
AR Path="/5FDB8EF8/60300FB9" Ref="R?"  Part="1" 
AR Path="/6003A36D/60300FB9" Ref="R21"  Part="1" 
F 0 "R21" V 5610 1300 50  0000 C CNN
F 1 "100k" V 5800 1300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5630 1300 50  0001 C CNN
F 3 "~" H 5700 1300 50  0001 C CNN
	1    5700 1300
	0    1    1    0   
$EndComp
Wire Wire Line
	4310 1300 4430 1300
Wire Wire Line
	5410 1300 5550 1300
Wire Wire Line
	5850 1300 6010 1300
Text Notes 3600 890  0    101  ~ 20
Boot Jumper
Wire Wire Line
	9410 1760 9410 1830
Wire Wire Line
	9410 1830 10040 1830
$Comp
L arduradio-rescue:SolderJumper_2_Open-Jumper JP5
U 1 1 60433451
P 9760 1420
F 0 "JP5" H 9760 1340 50  0000 C CNN
F 1 "SolderJumper_2_Open" V 9805 1488 50  0001 L CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 9760 1420 50  0001 C CNN
F 3 "~" H 9760 1420 50  0001 C CNN
	1    9760 1420
	-1   0    0    1   
$EndComp
Wire Wire Line
	9910 1420 10040 1420
Wire Wire Line
	10040 1420 10040 1300
Wire Wire Line
	9410 1420 9610 1420
Connection ~ 9410 1420
Wire Wire Line
	9410 1420 9410 1390
Wire Wire Line
	8750 1530 8750 1540
$Comp
L arduradio-rescue:VCC-power #PWR020
U 1 1 603A87EC
P 600 6650
F 0 "#PWR020" H 600 6500 50  0001 C CNN
F 1 "VCC" H 615 6823 50  0000 C CNN
F 2 "" H 600 6650 50  0001 C CNN
F 3 "" H 600 6650 50  0001 C CNN
	1    600  6650
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:VCC-power #PWR025
U 1 1 603AA764
P 2190 1580
F 0 "#PWR025" H 2190 1430 50  0001 C CNN
F 1 "VCC" H 2205 1753 50  0000 C CNN
F 2 "" H 2190 1580 50  0001 C CNN
F 3 "" H 2190 1580 50  0001 C CNN
	1    2190 1580
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:VCC-power #PWR033
U 1 1 603D9BD0
P 5170 880
F 0 "#PWR033" H 5170 730 50  0001 C CNN
F 1 "VCC" H 5185 1053 50  0000 C CNN
F 2 "" H 5170 880 50  0001 C CNN
F 3 "" H 5170 880 50  0001 C CNN
	1    5170 880 
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:VCC-power #PWR051
U 1 1 603F8D8E
P 8400 1940
F 0 "#PWR051" H 8400 1790 50  0001 C CNN
F 1 "VCC" H 8415 2113 50  0000 C CNN
F 2 "" H 8400 1940 50  0001 C CNN
F 3 "" H 8400 1940 50  0001 C CNN
	1    8400 1940
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:VCC-power #PWR053
U 1 1 60408906
P 9410 1390
F 0 "#PWR053" H 9410 1240 50  0001 C CNN
F 1 "VCC" H 9425 1563 50  0000 C CNN
F 2 "" H 9410 1390 50  0001 C CNN
F 3 "" H 9410 1390 50  0001 C CNN
	1    9410 1390
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:VCC-power #PWR055
U 1 1 60437C38
P 9670 2470
F 0 "#PWR055" H 9670 2320 50  0001 C CNN
F 1 "VCC" H 9685 2643 50  0000 C CNN
F 2 "" H 9670 2470 50  0001 C CNN
F 3 "" H 9670 2470 50  0001 C CNN
	1    9670 2470
	1    0    0    -1  
$EndComp
NoConn ~ 9570 2740
Text HLabel 8550 5340 0    50   Output ~ 0
AMP_MUTE
$Comp
L arduradio-rescue:R-Device R?
U 1 1 604B09D7
P 8830 5340
AR Path="/5FDB8EF8/604B09D7" Ref="R?"  Part="1" 
AR Path="/6003A36D/604B09D7" Ref="R29"  Part="1" 
F 0 "R29" V 8780 5140 50  0000 C CNN
F 1 "1k" V 8780 5520 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8760 5340 50  0001 C CNN
F 3 "~" H 8830 5340 50  0001 C CNN
	1    8830 5340
	0    1    1    0   
$EndComp
Wire Wire Line
	8550 5340 8680 5340
Text HLabel 8550 5440 0    50   Output ~ 0
BIAS_TEE_CONTROL
$Comp
L arduradio-rescue:R-Device R?
U 1 1 604C08B3
P 8830 5440
AR Path="/5FDB8EF8/604C08B3" Ref="R?"  Part="1" 
AR Path="/6003A36D/604C08B3" Ref="R30"  Part="1" 
F 0 "R30" V 8780 5240 50  0000 C CNN
F 1 "1k" V 8780 5620 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8760 5440 50  0001 C CNN
F 3 "~" H 8830 5440 50  0001 C CNN
	1    8830 5440
	0    1    1    0   
$EndComp
Wire Wire Line
	8550 5440 8680 5440
Wire Wire Line
	8980 5340 9070 5340
Wire Wire Line
	8980 5440 9070 5440
Text Label 10870 4940 0    50   ~ 0
TFT_DC
Text Label 10870 4840 0    50   ~ 0
TFT_RST
Text Label 4600 6390 2    50   ~ 0
TFT_CS
Connection ~ 4850 2590
Wire Wire Line
	4700 2590 4740 2590
Connection ~ 5550 5070
Connection ~ 5450 4720
Connection ~ 5350 4340
Connection ~ 5250 3990
Connection ~ 5150 3640
Connection ~ 5050 3290
Connection ~ 4950 2940
Wire Wire Line
	4700 5070 4740 5070
Wire Wire Line
	4700 4720 4740 4720
Wire Wire Line
	4700 4340 4740 4340
Wire Wire Line
	4700 3990 4740 3990
Wire Wire Line
	4700 3640 4740 3640
Wire Wire Line
	4700 3290 4740 3290
Wire Wire Line
	4700 2940 4740 2940
Wire Wire Line
	6560 5070 6660 5070
Text Label 4700 5070 2    50   ~ 0
AGC
Wire Wire Line
	6560 4720 6660 4720
Text Label 4700 4720 2    50   ~ 0
BIAS_SWITCH
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 6023841F
P 6660 5390
AR Path="/5FDB8EF8/6023841F" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/6023841F" Ref="#PWR045"  Part="1" 
AR Path="/6023841F" Ref="#PWR?"  Part="1" 
F 0 "#PWR045" H 6660 5140 50  0001 C CNN
F 1 "GND" H 6665 5217 50  0000 C CNN
F 2 "" H 6660 5390 50  0001 C CNN
F 3 "" H 6660 5390 50  0001 C CNN
	1    6660 5390
	1    0    0    -1  
$EndComp
Wire Wire Line
	6560 4340 6660 4340
Text Label 4700 4340 2    50   ~ 0
BFO_Switch
Wire Wire Line
	6560 3990 6660 3990
Text Label 4700 3990 2    50   ~ 0
Step_Switch
Wire Wire Line
	6560 3640 6660 3640
Text Label 4700 3640 2    50   ~ 0
Band_Button_Down
Wire Wire Line
	6560 3290 6660 3290
Text Label 4700 3290 2    50   ~ 0
Band_Button_Up
Wire Wire Line
	6560 2940 6660 2940
Text Label 4700 2940 2    50   ~ 0
Bandwidth_Button
$Comp
L arduradio-rescue:C-Device C?
U 1 1 60328AAA
P 5800 2740
AR Path="/5FDB8EF8/60328AAA" Ref="C?"  Part="1" 
AR Path="/6003A36D/60328AAA" Ref="C25"  Part="1" 
F 0 "C25" V 5930 2790 50  0000 L CNN
F 1 "100nF" V 5930 2500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5838 2590 50  0001 C CNN
F 3 "~" H 5800 2740 50  0001 C CNN
	1    5800 2740
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 60329DA0
P 5800 2400
AR Path="/5FDB8EF8/60329DA0" Ref="C?"  Part="1" 
AR Path="/6003A36D/60329DA0" Ref="C24"  Part="1" 
F 0 "C24" V 5930 2450 50  0000 L CNN
F 1 "100nF" V 5930 2160 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5838 2250 50  0001 C CNN
F 3 "~" H 5800 2400 50  0001 C CNN
	1    5800 2400
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 6032A290
P 5800 3090
AR Path="/5FDB8EF8/6032A290" Ref="C?"  Part="1" 
AR Path="/6003A36D/6032A290" Ref="C26"  Part="1" 
F 0 "C26" V 5930 3140 50  0000 L CNN
F 1 "100nF" V 5930 2850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5838 2940 50  0001 C CNN
F 3 "~" H 5800 3090 50  0001 C CNN
	1    5800 3090
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 6032A716
P 5800 3440
AR Path="/5FDB8EF8/6032A716" Ref="C?"  Part="1" 
AR Path="/6003A36D/6032A716" Ref="C27"  Part="1" 
F 0 "C27" V 5930 3490 50  0000 L CNN
F 1 "100nF" V 5930 3200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5838 3290 50  0001 C CNN
F 3 "~" H 5800 3440 50  0001 C CNN
	1    5800 3440
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 6032AADB
P 5800 3780
AR Path="/5FDB8EF8/6032AADB" Ref="C?"  Part="1" 
AR Path="/6003A36D/6032AADB" Ref="C28"  Part="1" 
F 0 "C28" V 5930 3830 50  0000 L CNN
F 1 "100nF" V 5930 3540 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5838 3630 50  0001 C CNN
F 3 "~" H 5800 3780 50  0001 C CNN
	1    5800 3780
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 6032B7E3
P 5800 4150
AR Path="/5FDB8EF8/6032B7E3" Ref="C?"  Part="1" 
AR Path="/6003A36D/6032B7E3" Ref="C29"  Part="1" 
F 0 "C29" V 5930 4200 50  0000 L CNN
F 1 "100nF" V 5930 3910 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5838 4000 50  0001 C CNN
F 3 "~" H 5800 4150 50  0001 C CNN
	1    5800 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	4850 2590 6160 2590
Wire Wire Line
	4950 2940 6160 2940
Wire Wire Line
	5050 3290 6160 3290
Wire Wire Line
	5150 3640 6160 3640
Wire Wire Line
	5250 3990 6160 3990
Wire Wire Line
	5350 4340 6160 4340
Wire Wire Line
	5450 4720 6160 4720
Wire Wire Line
	5550 5070 6160 5070
$Comp
L arduradio-rescue:C-Device C?
U 1 1 603FB022
P 5790 4510
AR Path="/5FDB8EF8/603FB022" Ref="C?"  Part="1" 
AR Path="/6003A36D/603FB022" Ref="C23"  Part="1" 
F 0 "C23" V 5920 4560 50  0000 L CNN
F 1 "100nF" V 5920 4270 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5828 4360 50  0001 C CNN
F 3 "~" H 5790 4510 50  0001 C CNN
	1    5790 4510
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 603FB50D
P 5800 4860
AR Path="/5FDB8EF8/603FB50D" Ref="C?"  Part="1" 
AR Path="/6003A36D/603FB50D" Ref="C30"  Part="1" 
F 0 "C30" V 5930 4910 50  0000 L CNN
F 1 "100nF" V 5930 4620 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5838 4710 50  0001 C CNN
F 3 "~" H 5800 4860 50  0001 C CNN
	1    5800 4860
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 2400 4850 2400
Wire Wire Line
	4850 2400 4850 2590
Wire Wire Line
	5650 2740 4950 2740
Wire Wire Line
	4950 2740 4950 2940
Wire Wire Line
	5650 3090 5050 3090
Wire Wire Line
	5050 3090 5050 3290
Wire Wire Line
	5650 3440 5150 3440
Wire Wire Line
	5150 3440 5150 3640
Wire Wire Line
	5650 3780 5250 3780
Wire Wire Line
	5250 3780 5250 3990
Wire Wire Line
	5650 4150 5350 4150
Wire Wire Line
	5350 4150 5350 4340
Wire Wire Line
	5640 4510 5450 4510
Wire Wire Line
	5450 4510 5450 4720
Wire Wire Line
	5650 4860 5550 4860
Wire Wire Line
	5550 4860 5550 5070
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 604B43CE
P 5990 4860
AR Path="/5FDB8EF8/604B43CE" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/604B43CE" Ref="#PWR044"  Part="1" 
AR Path="/604B43CE" Ref="#PWR?"  Part="1" 
F 0 "#PWR044" H 5990 4610 50  0001 C CNN
F 1 "GND" H 5995 4687 50  0000 C CNN
F 2 "" H 5990 4860 50  0001 C CNN
F 3 "" H 5990 4860 50  0001 C CNN
	1    5990 4860
	0    -1   -1   0   
$EndComp
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 604B4C96
P 5980 4510
AR Path="/5FDB8EF8/604B4C96" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/604B4C96" Ref="#PWR037"  Part="1" 
AR Path="/604B4C96" Ref="#PWR?"  Part="1" 
F 0 "#PWR037" H 5980 4260 50  0001 C CNN
F 1 "GND" H 5985 4337 50  0000 C CNN
F 2 "" H 5980 4510 50  0001 C CNN
F 3 "" H 5980 4510 50  0001 C CNN
	1    5980 4510
	0    -1   -1   0   
$EndComp
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 604B5057
P 5990 4150
AR Path="/5FDB8EF8/604B5057" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/604B5057" Ref="#PWR043"  Part="1" 
AR Path="/604B5057" Ref="#PWR?"  Part="1" 
F 0 "#PWR043" H 5990 3900 50  0001 C CNN
F 1 "GND" H 5995 3977 50  0000 C CNN
F 2 "" H 5990 4150 50  0001 C CNN
F 3 "" H 5990 4150 50  0001 C CNN
	1    5990 4150
	0    -1   -1   0   
$EndComp
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 604B54E4
P 5990 3780
AR Path="/5FDB8EF8/604B54E4" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/604B54E4" Ref="#PWR042"  Part="1" 
AR Path="/604B54E4" Ref="#PWR?"  Part="1" 
F 0 "#PWR042" H 5990 3530 50  0001 C CNN
F 1 "GND" H 5995 3607 50  0000 C CNN
F 2 "" H 5990 3780 50  0001 C CNN
F 3 "" H 5990 3780 50  0001 C CNN
	1    5990 3780
	0    -1   -1   0   
$EndComp
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 604B58DC
P 5990 3440
AR Path="/5FDB8EF8/604B58DC" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/604B58DC" Ref="#PWR041"  Part="1" 
AR Path="/604B58DC" Ref="#PWR?"  Part="1" 
F 0 "#PWR041" H 5990 3190 50  0001 C CNN
F 1 "GND" H 5995 3267 50  0000 C CNN
F 2 "" H 5990 3440 50  0001 C CNN
F 3 "" H 5990 3440 50  0001 C CNN
	1    5990 3440
	0    -1   -1   0   
$EndComp
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 604B5C92
P 5990 3090
AR Path="/5FDB8EF8/604B5C92" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/604B5C92" Ref="#PWR040"  Part="1" 
AR Path="/604B5C92" Ref="#PWR?"  Part="1" 
F 0 "#PWR040" H 5990 2840 50  0001 C CNN
F 1 "GND" H 5995 2917 50  0000 C CNN
F 2 "" H 5990 3090 50  0001 C CNN
F 3 "" H 5990 3090 50  0001 C CNN
	1    5990 3090
	0    -1   -1   0   
$EndComp
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 604B5F5F
P 5990 2740
AR Path="/5FDB8EF8/604B5F5F" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/604B5F5F" Ref="#PWR039"  Part="1" 
AR Path="/604B5F5F" Ref="#PWR?"  Part="1" 
F 0 "#PWR039" H 5990 2490 50  0001 C CNN
F 1 "GND" H 5995 2567 50  0000 C CNN
F 2 "" H 5990 2740 50  0001 C CNN
F 3 "" H 5990 2740 50  0001 C CNN
	1    5990 2740
	0    -1   -1   0   
$EndComp
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 604B63CF
P 5990 2400
AR Path="/5FDB8EF8/604B63CF" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/604B63CF" Ref="#PWR038"  Part="1" 
AR Path="/604B63CF" Ref="#PWR?"  Part="1" 
F 0 "#PWR038" H 5990 2150 50  0001 C CNN
F 1 "GND" H 5995 2227 50  0000 C CNN
F 2 "" H 5990 2400 50  0001 C CNN
F 3 "" H 5990 2400 50  0001 C CNN
	1    5990 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5950 2400 5990 2400
Wire Wire Line
	5950 2740 5990 2740
Wire Wire Line
	5950 3090 5990 3090
Wire Wire Line
	5950 3440 5990 3440
Wire Wire Line
	5950 3780 5990 3780
Wire Wire Line
	5950 4150 5990 4150
Wire Wire Line
	5940 4510 5980 4510
Wire Wire Line
	5950 4860 5990 4860
Text Label 7590 6010 2    50   ~ 0
RESET
$Comp
L arduradio-rescue:R-Device R?
U 1 1 6056A4F0
P 7470 5870
AR Path="/5FDB8EF8/6056A4F0" Ref="R?"  Part="1" 
AR Path="/6003A36D/6056A4F0" Ref="R22"  Part="1" 
F 0 "R22" V 7380 5750 50  0000 C CNN
F 1 "10k" V 7380 5940 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7400 5870 50  0001 C CNN
F 3 "~" H 7470 5870 50  0001 C CNN
	1    7470 5870
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 6056ABC4
P 8220 6220
AR Path="/5FDB8EF8/6056ABC4" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/6056ABC4" Ref="#PWR050"  Part="1" 
AR Path="/6056ABC4" Ref="#PWR?"  Part="1" 
F 0 "#PWR050" H 8220 5970 50  0001 C CNN
F 1 "GND" H 8225 6047 50  0000 C CNN
F 2 "" H 8220 6220 50  0001 C CNN
F 3 "" H 8220 6220 50  0001 C CNN
	1    8220 6220
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 6056B2A8
P 7940 6190
AR Path="/5FDB8EF8/6056B2A8" Ref="C?"  Part="1" 
AR Path="/6003A36D/6056B2A8" Ref="C35"  Part="1" 
F 0 "C35" V 8070 6240 50  0000 L CNN
F 1 "1uF" V 8070 5950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7978 6040 50  0001 C CNN
F 3 "~" H 7940 6190 50  0001 C CNN
	1    7940 6190
	0    1    1    0   
$EndComp
Wire Wire Line
	7590 6010 7660 6010
Wire Wire Line
	7660 6010 7660 6190
Connection ~ 7660 6010
Wire Wire Line
	7660 6010 7740 6010
Wire Wire Line
	8090 6190 8220 6190
Wire Wire Line
	8220 6190 8220 6220
Wire Wire Line
	8220 6190 8220 6010
Wire Wire Line
	8220 6010 8140 6010
Connection ~ 8220 6190
Wire Wire Line
	7660 6190 7790 6190
Wire Wire Line
	7620 5870 7660 5870
Wire Wire Line
	7660 5870 7660 6010
$Comp
L arduradio-rescue:VCC-power #PWR049
U 1 1 60467100
P 7590 5290
F 0 "#PWR049" H 7590 5140 50  0001 C CNN
F 1 "VCC" H 7605 5463 50  0000 C CNN
F 2 "" H 7590 5290 50  0001 C CNN
F 3 "" H 7590 5290 50  0001 C CNN
	1    7590 5290
	-1   0    0    1   
$EndComp
$Comp
L arduradio-rescue:VCC-power #PWR048
U 1 1 606BF7FB
P 7260 5930
F 0 "#PWR048" H 7260 5780 50  0001 C CNN
F 1 "VCC" H 7275 6103 50  0000 C CNN
F 2 "" H 7260 5930 50  0001 C CNN
F 3 "" H 7260 5930 50  0001 C CNN
	1    7260 5930
	-1   0    0    1   
$EndComp
Wire Wire Line
	7260 5930 7260 5870
Wire Wire Line
	7260 5870 7320 5870
Wire Wire Line
	6660 2590 6660 2940
Connection ~ 6660 2940
Wire Wire Line
	6660 2940 6660 3290
Connection ~ 6660 3290
Wire Wire Line
	6660 3290 6660 3640
Connection ~ 6660 3640
Wire Wire Line
	6660 3640 6660 3990
Connection ~ 6660 3990
Wire Wire Line
	6660 3990 6660 4340
Connection ~ 6660 4340
Wire Wire Line
	6660 4340 6660 4720
Connection ~ 6660 4720
Wire Wire Line
	6660 4720 6660 5070
Connection ~ 6660 5070
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60770DD6
P 4290 2760
AR Path="/5FDB8EF8/60770DD6" Ref="R?"  Part="1" 
AR Path="/6003A36D/60770DD6" Ref="R13"  Part="1" 
F 0 "R13" V 4200 2640 50  0000 C CNN
F 1 "10k" V 4200 2840 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4220 2760 50  0001 C CNN
F 3 "~" H 4290 2760 50  0001 C CNN
	1    4290 2760
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60770DDC
P 4290 3110
AR Path="/5FDB8EF8/60770DDC" Ref="R?"  Part="1" 
AR Path="/6003A36D/60770DDC" Ref="R14"  Part="1" 
F 0 "R14" V 4200 3010 50  0000 C CNN
F 1 "10k" V 4200 3190 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4220 3110 50  0001 C CNN
F 3 "~" H 4290 3110 50  0001 C CNN
	1    4290 3110
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60770DE2
P 4290 3460
AR Path="/5FDB8EF8/60770DE2" Ref="R?"  Part="1" 
AR Path="/6003A36D/60770DE2" Ref="R15"  Part="1" 
F 0 "R15" V 4200 3360 50  0000 C CNN
F 1 "10k" V 4200 3540 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4220 3460 50  0001 C CNN
F 3 "~" H 4290 3460 50  0001 C CNN
	1    4290 3460
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60770DE8
P 4290 3810
AR Path="/5FDB8EF8/60770DE8" Ref="R?"  Part="1" 
AR Path="/6003A36D/60770DE8" Ref="R16"  Part="1" 
F 0 "R16" V 4200 3710 50  0000 C CNN
F 1 "10k" V 4200 3890 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4220 3810 50  0001 C CNN
F 3 "~" H 4290 3810 50  0001 C CNN
	1    4290 3810
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60770DEE
P 4290 4160
AR Path="/5FDB8EF8/60770DEE" Ref="R?"  Part="1" 
AR Path="/6003A36D/60770DEE" Ref="R17"  Part="1" 
F 0 "R17" V 4200 4080 50  0000 C CNN
F 1 "10k" V 4200 4250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4220 4160 50  0001 C CNN
F 3 "~" H 4290 4160 50  0001 C CNN
	1    4290 4160
	0    1    1    0   
$EndComp
Wire Wire Line
	4740 2760 4440 2760
Wire Wire Line
	4440 3110 4740 3110
Wire Wire Line
	4440 3460 4740 3460
Wire Wire Line
	4440 3810 4740 3810
Wire Wire Line
	4440 4160 4740 4160
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60770DF9
P 4290 4540
AR Path="/5FDB8EF8/60770DF9" Ref="R?"  Part="1" 
AR Path="/6003A36D/60770DF9" Ref="R18"  Part="1" 
F 0 "R18" V 4200 4460 50  0000 C CNN
F 1 "10k" V 4200 4640 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4220 4540 50  0001 C CNN
F 3 "~" H 4290 4540 50  0001 C CNN
	1    4290 4540
	0    1    1    0   
$EndComp
Wire Wire Line
	4440 4540 4740 4540
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60770E00
P 4290 4890
AR Path="/5FDB8EF8/60770E00" Ref="R?"  Part="1" 
AR Path="/6003A36D/60770E00" Ref="R19"  Part="1" 
F 0 "R19" V 4200 4810 50  0000 C CNN
F 1 "10k" V 4200 4980 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4220 4890 50  0001 C CNN
F 3 "~" H 4290 4890 50  0001 C CNN
	1    4290 4890
	0    1    1    0   
$EndComp
Wire Wire Line
	4440 4890 4740 4890
Wire Wire Line
	4440 2410 4740 2410
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60770E08
P 4290 2410
AR Path="/5FDB8EF8/60770E08" Ref="R?"  Part="1" 
AR Path="/6003A36D/60770E08" Ref="R12"  Part="1" 
F 0 "R12" V 4200 2290 50  0000 C CNN
F 1 "10k" V 4200 2480 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4220 2410 50  0001 C CNN
F 3 "~" H 4290 2410 50  0001 C CNN
	1    4290 2410
	0    1    1    0   
$EndComp
Wire Wire Line
	4740 2410 4740 2590
Connection ~ 4740 2590
Wire Wire Line
	4740 2590 4850 2590
Wire Wire Line
	4740 2760 4740 2940
Connection ~ 4740 2940
Wire Wire Line
	4740 2940 4950 2940
Wire Wire Line
	4740 3110 4740 3290
Connection ~ 4740 3290
Wire Wire Line
	4740 3290 5050 3290
Wire Wire Line
	4740 3460 4740 3640
Connection ~ 4740 3640
Wire Wire Line
	4740 3640 5150 3640
Wire Wire Line
	4740 3810 4740 3990
Connection ~ 4740 3990
Wire Wire Line
	4740 3990 5250 3990
Wire Wire Line
	4740 4160 4740 4340
Connection ~ 4740 4340
Wire Wire Line
	4740 4340 5350 4340
Wire Wire Line
	4740 4540 4740 4720
Connection ~ 4740 4720
Wire Wire Line
	4740 4720 5450 4720
Wire Wire Line
	4740 4890 4740 5070
Connection ~ 4740 5070
Wire Wire Line
	4740 5070 5550 5070
$Comp
L arduradio-rescue:VCC-power #PWR031
U 1 1 6087F1BA
P 3750 2210
F 0 "#PWR031" H 3750 2060 50  0001 C CNN
F 1 "VCC" H 3765 2383 50  0000 C CNN
F 2 "" H 3750 2210 50  0001 C CNN
F 3 "" H 3750 2210 50  0001 C CNN
	1    3750 2210
	1    0    0    -1  
$EndComp
Wire Wire Line
	4140 2410 3750 2410
Wire Wire Line
	3750 2410 3750 2210
Wire Wire Line
	3750 2410 3750 2760
Wire Wire Line
	3750 2760 4140 2760
Connection ~ 3750 2410
Wire Wire Line
	3750 2760 3750 3110
Wire Wire Line
	3750 3110 4140 3110
Connection ~ 3750 2760
Wire Wire Line
	3750 3110 3750 3460
Wire Wire Line
	3750 3460 4140 3460
Connection ~ 3750 3110
Wire Wire Line
	3750 3460 3750 3810
Wire Wire Line
	3750 3810 4140 3810
Connection ~ 3750 3460
Wire Wire Line
	3750 3810 3750 4160
Wire Wire Line
	3750 4160 4140 4160
Connection ~ 3750 3810
Wire Wire Line
	3750 4160 3750 4540
Wire Wire Line
	3750 4540 4140 4540
Connection ~ 3750 4160
Wire Wire Line
	3750 4540 3750 4890
Wire Wire Line
	3750 4890 4140 4890
Connection ~ 3750 4540
$Comp
L arduradio-rescue:R-Device R?
U 1 1 6098CB57
P 1480 4750
AR Path="/5FDB8EF8/6098CB57" Ref="R?"  Part="1" 
AR Path="/6003A36D/6098CB57" Ref="R7"  Part="1" 
F 0 "R7" V 1390 4680 50  0000 C CNN
F 1 "10k" V 1390 4820 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1410 4750 50  0001 C CNN
F 3 "~" H 1480 4750 50  0001 C CNN
	1    1480 4750
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:R-Device R?
U 1 1 6098DB52
P 1480 5400
AR Path="/5FDB8EF8/6098DB52" Ref="R?"  Part="1" 
AR Path="/6003A36D/6098DB52" Ref="R8"  Part="1" 
F 0 "R8" V 1390 5320 50  0000 C CNN
F 1 "10k" V 1390 5470 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1410 5400 50  0001 C CNN
F 3 "~" H 1480 5400 50  0001 C CNN
	1    1480 5400
	0    -1   -1   0   
$EndComp
$Comp
L arduradio-rescue:VCC-power #PWR022
U 1 1 6098E133
P 800 4600
F 0 "#PWR022" H 800 4450 50  0001 C CNN
F 1 "VCC" H 815 4773 50  0000 C CNN
F 2 "" H 800 4600 50  0001 C CNN
F 3 "" H 800 4600 50  0001 C CNN
	1    800  4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1330 4750 800  4750
Wire Wire Line
	800  4750 800  4600
Wire Wire Line
	1330 5400 800  5400
Wire Wire Line
	800  5400 800  4750
Connection ~ 800  4750
Wire Wire Line
	1630 5400 1720 5400
Wire Wire Line
	1720 5400 1720 5100
Connection ~ 1720 5100
Wire Wire Line
	1720 5100 1730 5100
Wire Wire Line
	1630 4750 1710 4750
Wire Wire Line
	1710 4750 1710 4900
Connection ~ 1710 4900
Wire Wire Line
	1710 4900 1730 4900
$Comp
L arduradio-rescue:C-Device C?
U 1 1 60A1161B
P 2040 5400
AR Path="/5FDB8EF8/60A1161B" Ref="C?"  Part="1" 
AR Path="/6003A36D/60A1161B" Ref="C20"  Part="1" 
F 0 "C20" V 2170 5450 50  0000 L CNN
F 1 "100nF" V 2170 5160 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2078 5250 50  0001 C CNN
F 3 "~" H 2040 5400 50  0001 C CNN
	1    2040 5400
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 60A1267E
P 1940 4580
AR Path="/5FDB8EF8/60A1267E" Ref="C?"  Part="1" 
AR Path="/6003A36D/60A1267E" Ref="C18"  Part="1" 
F 0 "C18" V 2070 4630 50  0000 L CNN
F 1 "100nF" V 2070 4340 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1978 4430 50  0001 C CNN
F 3 "~" H 1940 4580 50  0001 C CNN
	1    1940 4580
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1710 4750 1710 4580
Wire Wire Line
	1710 4580 1790 4580
Connection ~ 1710 4750
Wire Wire Line
	1720 5400 1890 5400
Connection ~ 1720 5400
Wire Wire Line
	2190 5400 2410 5400
Connection ~ 2410 5400
Wire Wire Line
	2410 5400 2410 5540
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60ABFDDE
P 2660 4570
AR Path="/5FDB8EF8/60ABFDDE" Ref="R?"  Part="1" 
AR Path="/6003A36D/60ABFDDE" Ref="R10"  Part="1" 
F 0 "R10" V 2570 4480 50  0000 C CNN
F 1 "10k" V 2570 4640 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2590 4570 50  0001 C CNN
F 3 "~" H 2660 4570 50  0001 C CNN
	1    2660 4570
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2360 4900 2360 4570
Wire Wire Line
	2360 4570 2510 4570
Connection ~ 2360 4900
Wire Wire Line
	2360 4900 2440 4900
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 60B2CD93
P 2220 4380
AR Path="/5FDB8EF8/60B2CD93" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/60B2CD93" Ref="#PWR027"  Part="1" 
AR Path="/60B2CD93" Ref="#PWR?"  Part="1" 
F 0 "#PWR027" H 2220 4130 50  0001 C CNN
F 1 "GND" H 2225 4207 50  0000 C CNN
F 2 "" H 2220 4380 50  0001 C CNN
F 3 "" H 2220 4380 50  0001 C CNN
	1    2220 4380
	-1   0    0    1   
$EndComp
Wire Wire Line
	2090 4580 2220 4580
Wire Wire Line
	2220 4580 2220 4380
$Comp
L arduradio-rescue:VCC-power #PWR029
U 1 1 60B51D7C
P 2890 4500
F 0 "#PWR029" H 2890 4350 50  0001 C CNN
F 1 "VCC" H 2905 4673 50  0000 C CNN
F 2 "" H 2890 4500 50  0001 C CNN
F 3 "" H 2890 4500 50  0001 C CNN
	1    2890 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2810 4570 2890 4570
Wire Wire Line
	2890 4570 2890 4500
$Comp
L arduradio-rescue:C-Device C?
U 1 1 60B76FB4
P 2660 5220
AR Path="/5FDB8EF8/60B76FB4" Ref="C?"  Part="1" 
AR Path="/6003A36D/60B76FB4" Ref="C22"  Part="1" 
F 0 "C22" H 2780 5260 50  0000 L CNN
F 1 "100nF" H 2770 5170 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2698 5070 50  0001 C CNN
F 3 "~" H 2660 5220 50  0001 C CNN
	1    2660 5220
	1    0    0    -1  
$EndComp
Wire Wire Line
	2360 4900 2360 5020
Wire Wire Line
	2360 5020 2660 5020
Wire Wire Line
	2660 5020 2660 5070
Wire Wire Line
	2410 5400 2660 5400
Wire Wire Line
	2660 5400 2660 5370
Text Label 10870 5040 0    50   ~ 0
TFT_LED
$Comp
L arduradio-rescue:AO3401A-Transistor_FET Q1
U 1 1 60FED746
P 3350 6730
F 0 "Q1" H 3555 6730 50  0000 L CNN
F 1 "AO3401A" H 3555 6775 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3550 6655 50  0001 L CIN
F 3 "http://www.aosmd.com/pdfs/datasheet/AO3401A.pdf" H 3350 6730 50  0001 L CNN
	1    3350 6730
	1    0    0    1   
$EndComp
$Comp
L arduradio-rescue:VCC-power #PWR030
U 1 1 60FF2BDF
P 3450 6400
F 0 "#PWR030" H 3450 6250 50  0001 C CNN
F 1 "VCC" H 3465 6573 50  0000 C CNN
F 2 "" H 3450 6400 50  0001 C CNN
F 3 "" H 3450 6400 50  0001 C CNN
	1    3450 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 6530 3450 6460
Text Label 3000 6730 2    50   ~ 0
TFT_LED
Wire Wire Line
	3450 6930 3450 7090
Wire Wire Line
	3000 6730 3080 6730
$Comp
L arduradio-rescue:R-Device R?
U 1 1 6067803A
P 10620 5240
AR Path="/5FDB8EF8/6067803A" Ref="R?"  Part="1" 
AR Path="/6003A36D/6067803A" Ref="R38"  Part="1" 
F 0 "R38" V 10660 5060 50  0000 C CNN
F 1 "18" V 10660 5420 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10550 5240 50  0001 C CNN
F 3 "~" H 10620 5240 50  0001 C CNN
	1    10620 5240
	0    1    -1   0   
$EndComp
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60678034
P 10620 5140
AR Path="/5FDB8EF8/60678034" Ref="R?"  Part="1" 
AR Path="/6003A36D/60678034" Ref="R37"  Part="1" 
F 0 "R37" V 10570 4960 50  0000 C CNN
F 1 "18" V 10570 5320 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10550 5140 50  0001 C CNN
F 3 "~" H 10620 5140 50  0001 C CNN
	1    10620 5140
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:R-Device R?
U 1 1 6112988D
P 3080 6510
AR Path="/5FDB8EF8/6112988D" Ref="R?"  Part="1" 
AR Path="/6003A36D/6112988D" Ref="R11"  Part="1" 
F 0 "R11" H 3180 6460 50  0000 C CNN
F 1 "47k" H 3190 6550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3010 6510 50  0001 C CNN
F 3 "~" H 3080 6510 50  0001 C CNN
	1    3080 6510
	-1   0    0    1   
$EndComp
Wire Wire Line
	3080 6660 3080 6730
Connection ~ 3080 6730
Wire Wire Line
	3080 6730 3150 6730
Wire Wire Line
	3080 6360 3080 6310
Wire Wire Line
	3080 6310 3260 6310
Wire Wire Line
	3260 6310 3260 6460
Wire Wire Line
	3260 6460 3450 6460
Connection ~ 3450 6460
Wire Wire Line
	3450 6460 3450 6400
$Comp
L arduradio-rescue:R-Device R?
U 1 1 612B7A2E
P 10620 5040
AR Path="/5FDB8EF8/612B7A2E" Ref="R?"  Part="1" 
AR Path="/6003A36D/612B7A2E" Ref="R36"  Part="1" 
F 0 "R36" V 10570 4870 50  0000 C CNN
F 1 "1k" V 10570 5220 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10550 5040 50  0001 C CNN
F 3 "~" H 10620 5040 50  0001 C CNN
	1    10620 5040
	0    1    1    0   
$EndComp
Wire Wire Line
	10370 5040 10470 5040
Wire Wire Line
	10770 5040 10870 5040
$Comp
L arduradio-rescue:SolderJumper_3_Open-Jumper JP?
U 1 1 613332EC
P 4070 6790
AR Path="/5FD932E8/613332EC" Ref="JP?"  Part="1" 
AR Path="/6003A36D/613332EC" Ref="JP2"  Part="1" 
F 0 "JP2" V 4070 6857 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 4115 6857 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm_NumberLabels" H 4070 6790 50  0001 C CNN
F 3 "~" H 4070 6790 50  0001 C CNN
	1    4070 6790
	0    -1   1    0   
$EndComp
Wire Wire Line
	3450 6460 4070 6460
Wire Wire Line
	3450 7090 4070 7090
Wire Wire Line
	4070 7090 4070 6990
Wire Wire Line
	4070 6460 4070 6590
$Comp
L arduradio-rescue:R-Device R?
U 1 1 614FBCF5
P 8840 4340
AR Path="/5FDB8EF8/614FBCF5" Ref="R?"  Part="1" 
AR Path="/6003A36D/614FBCF5" Ref="R31"  Part="1" 
F 0 "R31" V 8790 4150 50  0000 C CNN
F 1 "1k" V 8790 4510 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8770 4340 50  0001 C CNN
F 3 "~" H 8840 4340 50  0001 C CNN
	1    8840 4340
	0    1    1    0   
$EndComp
Wire Wire Line
	8990 4340 9070 4340
Connection ~ 7670 3740
Connection ~ 7670 4350
Wire Wire Line
	7670 4350 8120 4350
Wire Wire Line
	7920 2830 8550 2830
Wire Wire Line
	7920 3440 9070 3440
Wire Wire Line
	7670 3740 9070 3740
Wire Wire Line
	8120 3840 9070 3840
Wire Wire Line
	8520 4340 8690 4340
$Comp
L arduradio-rescue:LED-Device D?
U 1 1 616767C5
P 10350 6020
AR Path="/60457E44/616767C5" Ref="D?"  Part="1" 
AR Path="/60457E44/60362EF5/616767C5" Ref="D?"  Part="1" 
AR Path="/6003A36D/616767C5" Ref="D4"  Part="1" 
F 0 "D4" H 10180 6090 50  0000 L CNN
F 1 "LED" H 10430 6080 50  0000 L CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 10350 6020 50  0001 C CNN
F 3 "~" H 10350 6020 50  0001 C CNN
	1    10350 6020
	1    0    0    -1  
$EndComp
Text Label 10690 6020 0    50   ~ 0
LED_1
Wire Wire Line
	10500 6020 10690 6020
Wire Wire Line
	10200 6020 9570 6020
Connection ~ 9570 6020
Wire Wire Line
	9570 6020 9570 5870
$Comp
L arduradio-rescue:CR2013-MI2120-Driver_Display U3
U 1 1 604667C9
P 5450 6590
F 0 "U3" H 4950 7160 50  0000 C CNN
F 1 "CR2013-MI2120" H 5910 7160 50  0000 C CNN
F 2 "Display:CR2013-MI2120" H 5450 5890 50  0001 C CNN
F 3 "http://pan.baidu.com/s/11Y990" H 4800 7090 50  0001 C CNN
	1    5450 6590
	-1   0    0    -1  
$EndComp
Text Label 4600 6490 2    50   ~ 0
MOSI
Text Label 4600 6890 2    50   ~ 0
TFT_RST
Text Label 4600 6690 2    50   ~ 0
CLK
Text Label 4600 6590 2    50   ~ 0
MISO
NoConn ~ 6250 6590
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 60591FE5
P 5450 7240
AR Path="/5FDB8EF8/60591FE5" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/60591FE5" Ref="#PWR036"  Part="1" 
AR Path="/60591FE5" Ref="#PWR?"  Part="1" 
F 0 "#PWR036" H 5450 6990 50  0001 C CNN
F 1 "GND" H 5455 7067 50  0000 C CNN
F 2 "" H 5450 7240 50  0001 C CNN
F 3 "" H 5450 7240 50  0001 C CNN
	1    5450 7240
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 7240 5450 7190
Wire Wire Line
	4600 6890 4650 6890
Wire Wire Line
	4600 6690 4650 6690
Wire Wire Line
	4600 6590 4650 6590
Wire Wire Line
	4600 6490 4650 6490
Wire Wire Line
	4600 6390 4650 6390
Text Label 4600 6290 2    50   ~ 0
TFT_DC
Wire Wire Line
	4650 6290 4600 6290
Text Notes 3730 5970 0    101  ~ 20
Display
Wire Wire Line
	4220 6790 4650 6790
Wire Wire Line
	4730 1300 4910 1300
$Comp
L arduradio-rescue:SolderJumper_3_Open-Jumper JP3
U 1 1 609DCEB1
P 5060 1300
F 0 "JP3" V 4890 1130 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 5105 1368 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm" H 5060 1300 50  0001 C CNN
F 3 "~" H 5060 1300 50  0001 C CNN
	1    5060 1300
	0    1    1    0   
$EndComp
$Comp
L arduradio-rescue:SolderJumper_3_Open-Jumper JP4
U 1 1 60BDCCBF
P 5260 1300
F 0 "JP4" V 5090 1130 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 5305 1368 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm" H 5260 1300 50  0001 C CNN
F 3 "~" H 5260 1300 50  0001 C CNN
	1    5260 1300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5060 1100 5060 1030
Wire Wire Line
	5060 1030 5170 1030
Wire Wire Line
	5170 880  5170 1030
Wire Wire Line
	5170 1030 5260 1030
Wire Wire Line
	5260 1030 5260 1100
Connection ~ 5170 1030
Wire Wire Line
	5060 1500 5060 1560
Wire Wire Line
	5060 1560 5170 1560
Wire Wire Line
	5170 1560 5170 1610
Wire Wire Line
	5170 1560 5260 1560
Wire Wire Line
	5260 1560 5260 1500
Connection ~ 5170 1560
$Comp
L arduradio-rescue:SW_Push-Switch SW10
U 1 1 605699AD
P 7940 6010
F 0 "SW10" H 7940 6190 50  0000 C CNN
F 1 "SW_Push" H 8050 6160 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_SPST_SKQG_WithStem" H 7940 6210 50  0001 C CNN
F 3 "~" H 7940 6210 50  0001 C CNN
	1    7940 6010
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:SW_Push-Switch SW9
U 1 1 602DA46B
P 6360 5070
F 0 "SW9" H 6360 5250 50  0000 C CNN
F 1 "SW_Push" H 6470 5220 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_SPST_SKQG_WithStem" H 6360 5270 50  0001 C CNN
F 3 "~" H 6360 5270 50  0001 C CNN
	1    6360 5070
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:SW_Push-Switch SW8
U 1 1 602D4EC4
P 6360 4720
F 0 "SW8" H 6360 4900 50  0000 C CNN
F 1 "SW_Push" H 6470 4870 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_SPST_SKQG_WithStem" H 6360 4920 50  0001 C CNN
F 3 "~" H 6360 4920 50  0001 C CNN
	1    6360 4720
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:SW_Push-Switch SW7
U 1 1 60216D91
P 6360 4340
F 0 "SW7" H 6360 4520 50  0000 C CNN
F 1 "SW_Push" H 6470 4490 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_SPST_SKQG_WithStem" H 6360 4540 50  0001 C CNN
F 3 "~" H 6360 4540 50  0001 C CNN
	1    6360 4340
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:SW_Push-Switch SW6
U 1 1 60214213
P 6360 3990
F 0 "SW6" H 6360 4170 50  0000 C CNN
F 1 "SW_Push" H 6470 4140 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_SPST_SKQG_WithStem" H 6360 4190 50  0001 C CNN
F 3 "~" H 6360 4190 50  0001 C CNN
	1    6360 3990
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:SW_Push-Switch SW5
U 1 1 60211C44
P 6360 3640
F 0 "SW5" H 6360 3820 50  0000 C CNN
F 1 "SW_Push" H 6470 3790 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_SPST_SKQG_WithStem" H 6360 3840 50  0001 C CNN
F 3 "~" H 6360 3840 50  0001 C CNN
	1    6360 3640
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:SW_Push-Switch SW4
U 1 1 6020F089
P 6360 3290
F 0 "SW4" H 6360 3470 50  0000 C CNN
F 1 "SW_Push" H 6470 3440 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_SPST_SKQG_WithStem" H 6360 3490 50  0001 C CNN
F 3 "~" H 6360 3490 50  0001 C CNN
	1    6360 3290
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:SW_Push-Switch SW3
U 1 1 6020C032
P 6360 2940
F 0 "SW3" H 6360 3120 50  0000 C CNN
F 1 "SW_Push" H 6470 3090 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_SPST_SKQG_WithStem" H 6360 3140 50  0001 C CNN
F 3 "~" H 6360 3140 50  0001 C CNN
	1    6360 2940
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:SW_Push-Switch SW2
U 1 1 601F40CA
P 6360 2590
F 0 "SW2" H 6360 2770 50  0000 C CNN
F 1 "SW_Push" H 6470 2740 50  0001 C CNN
F 2 "Button_Switch_SMD:SW_SPST_SKQG_WithStem" H 6360 2790 50  0001 C CNN
F 3 "~" H 6360 2790 50  0001 C CNN
	1    6360 2590
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 5990 5450 5940
Text Label 10870 4340 0    50   ~ 0
SD_CS
Text Label 6340 6190 0    50   ~ 0
SD_CS
Text Label 6340 6290 0    50   ~ 0
MOSI
Text Label 6340 6390 0    50   ~ 0
MISO
Text Label 6340 6490 0    50   ~ 0
CLK
Wire Wire Line
	6250 6190 6340 6190
Wire Wire Line
	6250 6290 6340 6290
Wire Wire Line
	6250 6390 6340 6390
Wire Wire Line
	6250 6490 6340 6490
Text Label 10460 4140 0    50   ~ 0
Mode_Switch
Wire Wire Line
	10370 4140 10460 4140
Text Label 10460 4240 0    50   ~ 0
Bandwidth_Button
Wire Wire Line
	10370 4240 10460 4240
Text Label 9000 4440 2    50   ~ 0
Band_Button_Up
Text Label 9000 4140 2    50   ~ 0
Band_Button_Down
Wire Wire Line
	9000 4440 9070 4440
Wire Wire Line
	9000 4140 9070 4140
Text Label 8520 4940 2    50   ~ 0
Step_Switch
Wire Wire Line
	8520 4940 9070 4940
Text Label 8580 5540 2    50   ~ 0
BFO_Switch
Wire Wire Line
	8580 5540 9070 5540
Text Label 10520 5540 0    50   ~ 0
BIAS_SWITCH
Wire Wire Line
	10370 5540 10520 5540
Text Label 8990 3640 2    50   ~ 0
AGC
Wire Wire Line
	8990 3640 9070 3640
$Comp
L arduradio-rescue:VCC-power #PWR035
U 1 1 60591204
P 5450 5940
F 0 "#PWR035" H 5450 5790 50  0001 C CNN
F 1 "VCC" V 5465 6067 50  0000 L CNN
F 2 "" H 5450 5940 50  0001 C CNN
F 3 "" H 5450 5940 50  0001 C CNN
	1    5450 5940
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 2590 4850 5390
Wire Wire Line
	4950 2940 4950 5390
Wire Wire Line
	5050 3290 5050 5390
Wire Wire Line
	5150 3640 5150 5390
Wire Wire Line
	5250 3990 5250 5390
Wire Wire Line
	5350 4340 5350 5390
Wire Wire Line
	5450 4720 5450 5390
Wire Wire Line
	5550 5070 5550 5390
Text Label 4750 5350 1    50   ~ 0
RESET
Wire Wire Line
	4750 5350 4750 5390
$Comp
L Connector_Generic:Conn_01x10 J10
U 1 1 60BE8338
P 5050 5590
F 0 "J10" V 5050 4940 50  0000 C CNN
F 1 "Conn_01x10" V 5266 5536 50  0001 C CNN
F 2 "Connector_JST:JST_PH_B10B-PH-K_1x10_P2.00mm_Vertical" H 5050 5590 50  0001 C CNN
F 3 "~" H 5050 5590 50  0001 C CNN
	1    5050 5590
	0    -1   1    0   
$EndComp
Wire Wire Line
	6660 5070 6660 5390
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 60EECDB5
P 4390 5360
AR Path="/5FDB8EF8/60EECDB5" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/60EECDB5" Ref="#PWR032"  Part="1" 
AR Path="/60EECDB5" Ref="#PWR?"  Part="1" 
F 0 "#PWR032" H 4390 5110 50  0001 C CNN
F 1 "GND" H 4395 5187 50  0000 C CNN
F 2 "" H 4390 5360 50  0001 C CNN
F 3 "" H 4390 5360 50  0001 C CNN
	1    4390 5360
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 5390 4650 5280
Wire Wire Line
	4650 5280 4390 5280
Wire Wire Line
	4390 5280 4390 5360
Wire Wire Line
	10370 4340 10870 4340
Wire Wire Line
	10370 4440 10870 4440
Wire Wire Line
	10370 4540 10870 4540
Wire Wire Line
	10370 4640 10870 4640
Wire Wire Line
	10370 4740 10870 4740
Wire Wire Line
	10370 4840 10870 4840
Wire Wire Line
	10370 4940 10870 4940
Text Label 8520 4340 2    50   ~ 0
LED_1
Wire Wire Line
	9570 6020 9570 6310
Text HLabel 8560 4040 0    50   Input ~ 0
Bat_Sens
Wire Wire Line
	8560 4040 9070 4040
Wire Wire Line
	8320 1530 8750 1530
Wire Wire Line
	7900 1530 8020 1530
$Comp
L arduradio-rescue:D_Schottky-Device D3
U 1 1 6027C26A
P 8170 1530
F 0 "D3" H 8170 1420 50  0000 C CNN
F 1 "SS34" H 8020 1410 50  0001 C CNN
F 2 "Diode_SMD:D_SMA" H 8170 1530 50  0001 C CNN
F 3 "~" H 8170 1530 50  0001 C CNN
	1    8170 1530
	-1   0    0    1   
$EndComp
$EndSCHEMATC
