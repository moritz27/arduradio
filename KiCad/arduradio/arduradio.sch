EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Notes Line
	4290 6540 6970 6540
Wire Notes Line
	4280 6550 4280 7800
Text Notes 5060 6740 0    50   ~ 0
Mounting Holes
$Comp
L arduradio-rescue:GND-power #PWR?
U 1 1 5FF32241
P 4960 7530
AR Path="/5FDB8EF8/5FF32241" Ref="#PWR?"  Part="1" 
AR Path="/5FF32241" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 4960 7280 50  0001 C CNN
F 1 "GND" H 4965 7357 50  0000 C CNN
F 2 "" H 4960 7530 50  0001 C CNN
F 3 "" H 4960 7530 50  0001 C CNN
	1    4960 7530
	1    0    0    -1  
$EndComp
Wire Wire Line
	4960 7530 5260 7530
Wire Wire Line
	5260 7530 5260 7450
Wire Wire Line
	5260 7530 5960 7530
Wire Wire Line
	5960 7530 5960 7450
Connection ~ 5260 7530
Wire Wire Line
	5240 7120 4960 7120
Wire Wire Line
	4960 7120 4960 7530
Connection ~ 4960 7530
Wire Wire Line
	5960 7120 5240 7120
Connection ~ 5240 7120
$Comp
L arduradio-rescue:MountingHole_Pad-Mechanical H?
U 1 1 5FF32251
P 5960 7350
AR Path="/5FDB8EF8/5FF32251" Ref="H?"  Part="1" 
AR Path="/5FF32251" Ref="H4"  Part="1" 
F 0 "H4" H 6060 7396 50  0000 L CNN
F 1 "MountingHole" H 6060 7305 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad_Via" H 5960 7350 50  0001 C CNN
F 3 "~" H 5960 7350 50  0001 C CNN
	1    5960 7350
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:MountingHole_Pad-Mechanical H?
U 1 1 5FF32257
P 5260 7350
AR Path="/5FDB8EF8/5FF32257" Ref="H?"  Part="1" 
AR Path="/5FF32257" Ref="H2"  Part="1" 
F 0 "H2" H 5360 7396 50  0000 L CNN
F 1 "MountingHole" H 5360 7305 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad_Via" H 5260 7350 50  0001 C CNN
F 3 "~" H 5260 7350 50  0001 C CNN
	1    5260 7350
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:MountingHole_Pad-Mechanical H?
U 1 1 5FF3225D
P 5960 7020
AR Path="/5FDB8EF8/5FF3225D" Ref="H?"  Part="1" 
AR Path="/5FF3225D" Ref="H3"  Part="1" 
F 0 "H3" H 6060 7066 50  0000 L CNN
F 1 "MountingHole" H 6060 6975 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad_Via" H 5960 7020 50  0001 C CNN
F 3 "~" H 5960 7020 50  0001 C CNN
	1    5960 7020
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:MountingHole_Pad-Mechanical H?
U 1 1 5FF32263
P 5240 7020
AR Path="/5FDB8EF8/5FF32263" Ref="H?"  Part="1" 
AR Path="/5FF32263" Ref="H1"  Part="1" 
F 0 "H1" H 5340 7066 50  0000 L CNN
F 1 "MountingHole" H 5340 6975 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad_Via" H 5240 7020 50  0001 C CNN
F 3 "~" H 5240 7020 50  0001 C CNN
	1    5240 7020
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:Conn_01x02-Connector_Generic J5
U 1 1 5FFCB679
P 8730 2030
F 0 "J5" H 8810 2022 50  0000 L CNN
F 1 "Conn_01x02" H 8810 1931 50  0001 L CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 8730 2030 50  0001 C CNN
F 3 "~" H 8730 2030 50  0001 C CNN
	1    8730 2030
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:Conn_01x02-Connector_Generic J6
U 1 1 5FFCC124
P 8740 2610
F 0 "J6" H 8820 2602 50  0000 L CNN
F 1 "Conn_01x02" H 8820 2511 50  0001 L CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 8740 2610 50  0001 C CNN
F 3 "~" H 8740 2610 50  0001 C CNN
	1    8740 2610
	1    0    0    1   
$EndComp
$Comp
L arduradio-rescue:Conn_Coaxial-Connector J?
U 1 1 601DD33B
P 6750 2470
AR Path="/5FD932E8/601DD33B" Ref="J?"  Part="1" 
AR Path="/601DD33B" Ref="J1"  Part="1" 
F 0 "J1" H 6570 2390 50  0000 L CNN
F 1 "Conn_Coaxial" H 6850 2354 50  0001 L CNN
F 2 "Connector_Coaxial:SMA_Molex_73251-1153_EdgeMount_Horizontal" H 6750 2470 50  0001 C CNN
F 3 " ~" H 6750 2470 50  0001 C CNN
	1    6750 2470
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:Conn_Coaxial-Connector J?
U 1 1 601DD341
P 8480 4160
AR Path="/5FD932E8/601DD341" Ref="J?"  Part="1" 
AR Path="/601DD341" Ref="J3"  Part="1" 
F 0 "J3" H 8580 4089 50  0000 L CNN
F 1 "Conn_Coaxial" H 8580 4044 50  0001 L CNN
F 2 "Connector_Coaxial:SMA_Molex_73251-1153_EdgeMount_Horizontal" H 8480 4160 50  0001 C CNN
F 3 " ~" H 8480 4160 50  0001 C CNN
	1    8480 4160
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 2170 5300 2170
Wire Wire Line
	5300 2170 5300 2100
Wire Wire Line
	7340 2030 7190 2030
Wire Wire Line
	7340 2630 7290 2630
Wire Wire Line
	7340 2480 7290 2480
Wire Wire Line
	8090 2510 8310 2510
Wire Wire Line
	6190 2180 7340 2180
$Sheet
S 7340 1930 750  850 
U 5FD94276
F0 "PAM8403" 50
F1 "PAM8403.sch" 50
F2 "INL" I L 7340 2180 50 
F3 "INR" I L 7340 2330 50 
F4 "MUTE" I L 7340 2480 50 
F5 "SHDN" I L 7340 2630 50 
F6 "Vin" I L 7340 2030 50 
F7 "OUT_R_-" O R 8090 2510 50 
F8 "OUT_R_+" O R 8090 2400 50 
F9 "OUT_L_+" O R 8090 2230 50 
F10 "OUT_L_-" O R 8090 2130 50 
$EndSheet
Wire Wire Line
	7340 2330 6190 2330
$Comp
L arduradio-rescue:Conn_Coaxial-Connector J?
U 1 1 601EAAA1
P 8480 4490
AR Path="/5FD932E8/601EAAA1" Ref="J?"  Part="1" 
AR Path="/601EAAA1" Ref="J4"  Part="1" 
F 0 "J4" H 8580 4419 50  0000 L CNN
F 1 "Conn_Coaxial" H 8580 4374 50  0001 L CNN
F 2 "arduradio:U.FL_IPX_Molex" H 8480 4490 50  0001 C CNN
F 3 " ~" H 8480 4490 50  0001 C CNN
	1    8480 4490
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:Conn_Coaxial-Connector J?
U 1 1 601ECABB
P 6750 2790
AR Path="/5FD932E8/601ECABB" Ref="J?"  Part="1" 
AR Path="/601ECABB" Ref="J2"  Part="1" 
F 0 "J2" H 6570 2720 50  0000 L CNN
F 1 "Conn_Coaxial" H 6850 2674 50  0001 L CNN
F 2 "arduradio:U.FL_IPX_Molex" H 6750 2790 50  0001 C CNN
F 3 " ~" H 6750 2790 50  0001 C CNN
	1    6750 2790
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 2790 6480 2790
Wire Wire Line
	6480 2790 6480 2470
Wire Wire Line
	6480 2470 6550 2470
Wire Wire Line
	8280 4490 8210 4490
Wire Wire Line
	8210 4490 8210 4160
Wire Wire Line
	8210 4160 8280 4160
Wire Wire Line
	6190 2470 6480 2470
Connection ~ 6480 2470
Wire Wire Line
	6750 2670 6960 2670
Wire Wire Line
	6960 2670 6960 3060
Wire Wire Line
	6960 3060 6840 3060
Wire Wire Line
	6750 3060 6750 2990
Text Label 7290 2480 2    50   ~ 0
AMP_Mute
$Comp
L arduradio-rescue:GND-power #PWR07
U 1 1 601FACE6
P 6960 3090
F 0 "#PWR07" H 6960 2840 50  0001 C CNN
F 1 "GND" H 6965 2917 50  0000 C CNN
F 2 "" H 6960 3090 50  0001 C CNN
F 3 "" H 6960 3090 50  0001 C CNN
	1    6960 3090
	1    0    0    -1  
$EndComp
Wire Wire Line
	6960 3090 6960 3060
Connection ~ 6960 3060
$Comp
L arduradio-rescue:AudioJack2_Ground_Switch-Connector J7
U 1 1 601FD949
P 9770 2400
F 0 "J7" H 9590 2372 50  0000 R CNN
F 1 "AudioJack2_Ground_Switch" H 9590 2327 50  0001 R CNN
F 2 "arduradio:Audio_Jack" H 9770 2600 50  0001 C CNN
F 3 "~" H 9770 2600 50  0001 C CNN
	1    9770 2400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8090 2230 8170 2230
Wire Wire Line
	8170 2230 8170 2340
Wire Wire Line
	8170 2340 9460 2340
Wire Wire Line
	9460 2200 9460 2340
Wire Wire Line
	9460 2200 9570 2200
Wire Wire Line
	8090 2400 9570 2400
Wire Wire Line
	9570 2500 9380 2500
Wire Wire Line
	9380 2500 9380 2740
Wire Wire Line
	9380 2740 8400 2740
Wire Wire Line
	8400 2740 8400 2610
Wire Wire Line
	8400 2610 8540 2610
Wire Wire Line
	8530 2130 8310 2130
Wire Wire Line
	8530 2030 8440 2030
Wire Wire Line
	8440 2030 8440 1910
Wire Wire Line
	8440 1910 9330 1910
Wire Wire Line
	9330 1910 9330 2300
Wire Wire Line
	9330 2300 9570 2300
Wire Wire Line
	8310 2130 8310 1710
Wire Wire Line
	8310 1710 10250 1710
Wire Wire Line
	10250 1710 10250 2530
Connection ~ 8310 2130
Wire Wire Line
	8310 2130 8090 2130
Wire Wire Line
	8310 2510 8310 3020
Wire Wire Line
	8310 3020 10250 3020
Wire Wire Line
	10250 3020 10250 2930
Connection ~ 8310 2510
Wire Wire Line
	8310 2510 8540 2510
$Comp
L arduradio-rescue:SolderJumper_3_Open-Jumper JP1
U 1 1 60212EFB
P 10250 2730
F 0 "JP1" V 10204 2798 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 10295 2798 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm" H 10250 2730 50  0001 C CNN
F 3 "~" H 10250 2730 50  0001 C CNN
	1    10250 2730
	0    1    1    0   
$EndComp
Wire Wire Line
	9770 2700 9770 2730
Wire Wire Line
	9770 2730 10100 2730
$Comp
L arduradio:FDC6329L U1
U 1 1 60215CBE
P 5960 4770
F 0 "U1" H 6530 5160 50  0000 C CNN
F 1 "FDC6329L" H 6230 5160 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 6410 4670 50  0001 C CNN
F 3 "" H 5960 4770 50  0001 C CNN
	1    5960 4770
	-1   0    0    -1  
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 6021E9AB
P 3840 5020
AR Path="/5FD94276/6021E9AB" Ref="C?"  Part="1" 
AR Path="/5FD932E8/6021E9AB" Ref="C?"  Part="1" 
AR Path="/6021E9AB" Ref="C1"  Part="1" 
F 0 "C1" H 3970 4980 50  0000 L CNN
F 1 "100nF" H 3970 5090 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3878 4870 50  0001 C CNN
F 3 "~" H 3840 5020 50  0001 C CNN
	1    3840 5020
	1    0    0    1   
$EndComp
Connection ~ 3840 4720
Wire Wire Line
	3840 4720 3840 4870
Text Label 4880 4620 2    50   ~ 0
BIAS_TEE_CONTROL
Wire Wire Line
	5160 4720 3840 4720
$Comp
L arduradio-rescue:GND-power #PWR01
U 1 1 602349EC
P 3840 5240
F 0 "#PWR01" H 3840 4990 50  0001 C CNN
F 1 "GND" H 3845 5067 50  0000 C CNN
F 2 "" H 3840 5240 50  0001 C CNN
F 3 "" H 3840 5240 50  0001 C CNN
	1    3840 5240
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3840 5240 3840 5170
Wire Wire Line
	8480 4360 8890 4360
Wire Wire Line
	8890 4360 8890 4850
Wire Wire Line
	8890 4850 8480 4850
Wire Wire Line
	8480 4850 8480 4690
$Comp
L arduradio-rescue:GND-power #PWR08
U 1 1 60274D5B
P 8890 4920
F 0 "#PWR08" H 8890 4670 50  0001 C CNN
F 1 "GND" H 8895 4747 50  0000 C CNN
F 2 "" H 8890 4920 50  0001 C CNN
F 3 "" H 8890 4920 50  0001 C CNN
	1    8890 4920
	1    0    0    -1  
$EndComp
Wire Wire Line
	8890 4920 8890 4850
Connection ~ 8890 4850
Wire Wire Line
	6190 2610 6390 2610
Wire Wire Line
	6390 2610 6390 4090
$Comp
L arduradio-rescue:L-Device L?
U 1 1 60282F08
P 7740 4620
AR Path="/5FD94276/60282F08" Ref="L?"  Part="1" 
AR Path="/60282F08" Ref="L1"  Part="1" 
F 0 "L1" V 7860 4700 50  0000 C CNN
F 1 "680uH" V 7850 4500 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H3.5" H 7740 4620 50  0001 C CNN
F 3 "~" H 7740 4620 50  0001 C CNN
	1    7740 4620
	0    1    -1   0   
$EndComp
Wire Wire Line
	6060 4620 6220 4620
Wire Wire Line
	6060 4720 6220 4720
Wire Wire Line
	6220 4720 6220 4620
Connection ~ 6220 4620
Wire Wire Line
	6370 5010 6370 5080
$Comp
L arduradio-rescue:GND-power #PWR05
U 1 1 6028C9D7
P 6370 5130
F 0 "#PWR05" H 6370 4880 50  0001 C CNN
F 1 "GND" H 6375 4957 50  0000 C CNN
F 2 "" H 6370 5130 50  0001 C CNN
F 3 "" H 6370 5130 50  0001 C CNN
	1    6370 5130
	1    0    0    -1  
$EndComp
Wire Wire Line
	8210 4090 8210 4160
Connection ~ 8210 4160
Wire Wire Line
	7890 4620 8000 4620
Wire Wire Line
	8210 4620 8210 4490
Connection ~ 8210 4490
$Comp
L arduradio-rescue:C-Device C?
U 1 1 6029C055
P 6830 4870
AR Path="/5FD94276/6029C055" Ref="C?"  Part="1" 
AR Path="/5FD932E8/6029C055" Ref="C?"  Part="1" 
AR Path="/6029C055" Ref="C4"  Part="1" 
F 0 "C4" H 6600 4830 50  0000 L CNN
F 1 "1nF" H 6570 4920 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6868 4720 50  0001 C CNN
F 3 "~" H 6830 4870 50  0001 C CNN
	1    6830 4870
	-1   0    0    -1  
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 602A0A57
P 7300 4870
AR Path="/5FD94276/602A0A57" Ref="C?"  Part="1" 
AR Path="/5FD932E8/602A0A57" Ref="C?"  Part="1" 
AR Path="/602A0A57" Ref="C5"  Part="1" 
F 0 "C5" H 7070 4830 50  0000 L CNN
F 1 "10nF" H 6990 4910 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7338 4720 50  0001 C CNN
F 3 "~" H 7300 4870 50  0001 C CNN
	1    7300 4870
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6370 4710 6370 4620
Wire Wire Line
	6220 4620 6370 4620
Connection ~ 6370 4620
Wire Wire Line
	6370 4620 6830 4620
Wire Wire Line
	6830 4720 6830 4620
Connection ~ 6830 4620
Wire Wire Line
	6830 4620 7300 4620
Wire Wire Line
	7300 4720 7300 4620
Connection ~ 7300 4620
Wire Wire Line
	7300 4620 7590 4620
Wire Wire Line
	6370 5080 6830 5080
Wire Wire Line
	6830 5080 6830 5020
Connection ~ 6370 5080
Wire Wire Line
	6370 5080 6370 5130
Wire Wire Line
	6830 5080 7300 5080
Wire Wire Line
	7300 5080 7300 5020
Connection ~ 6830 5080
Wire Wire Line
	4510 2320 5500 2320
Wire Wire Line
	4510 2470 5500 2470
Wire Wire Line
	4510 2620 5500 2620
$Sheet
S 3320 1910 1190 1300
U 6003A36D
F0 "STM32F103C8" 50
F1 "STM32F103C8.sch" 50
F2 "SI473X_INT" I R 4510 2770 50 
F3 "SI473X_RESET" O R 4510 2620 50 
F4 "SCLK" O R 4510 2470 50 
F5 "SDIO" B R 4510 2320 50 
F6 "AMP_MUTE" O R 4510 2920 50 
F7 "BIAS_TEE_CONTROL" O R 4510 3060 50 
F8 "Bat_Sens" I R 4510 2165 50 
$EndSheet
NoConn ~ 5500 2930
Wire Wire Line
	5500 2770 4510 2770
$Sheet
S 5280 1010 1110 290 
U 60457E44
F0 "power_management" 50
F1 "power_management.sch" 50
F2 "Vbat" O R 6390 1110 50 
F3 "SHDN" O R 6390 1210 50 
$EndSheet
$Comp
L arduradio-rescue:C-Device C?
U 1 1 60287B88
P 6370 4860
AR Path="/5FD94276/60287B88" Ref="C?"  Part="1" 
AR Path="/5FD932E8/60287B88" Ref="C?"  Part="1" 
AR Path="/60287B88" Ref="C3"  Part="1" 
F 0 "C3" H 6140 4820 50  0000 L CNN
F 1 "100pF" H 6000 4910 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6408 4710 50  0001 C CNN
F 3 "~" H 6370 4860 50  0001 C CNN
	1    6370 4860
	-1   0    0    -1  
$EndComp
$Comp
L arduradio-rescue:VCC-power #PWR?
U 1 1 6034A6B8
P 5300 2100
AR Path="/60457E44/6034A6B8" Ref="#PWR?"  Part="1" 
AR Path="/6034A6B8" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 5300 1950 50  0001 C CNN
F 1 "VCC" H 5315 2273 50  0000 C CNN
F 2 "" H 5300 2100 50  0001 C CNN
F 3 "" H 5300 2100 50  0001 C CNN
	1    5300 2100
	1    0    0    -1  
$EndComp
$Comp
L arduradio-rescue:C-Device C?
U 1 1 6047854C
P 5580 4190
AR Path="/5FD94276/6047854C" Ref="C?"  Part="1" 
AR Path="/5FD932E8/6047854C" Ref="C?"  Part="1" 
AR Path="/6047854C" Ref="C2"  Part="1" 
F 0 "C2" V 5730 4050 50  0000 L CNN
F 1 "100pF" V 5730 4190 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5618 4040 50  0001 C CNN
F 3 "~" H 5580 4190 50  0001 C CNN
	1    5580 4190
	0    1    -1   0   
$EndComp
$Comp
L arduradio-rescue:R-Device R?
U 1 1 6047AF9A
P 6290 4520
AR Path="/5FD94276/6047AF9A" Ref="R?"  Part="1" 
AR Path="/6047AF9A" Ref="R4"  Part="1" 
F 0 "R4" V 6370 4560 50  0000 L CNN
F 1 "2k2" V 6370 4390 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6220 4520 50  0001 C CNN
F 3 "~" H 6290 4520 50  0001 C CNN
	1    6290 4520
	0    1    -1   0   
$EndComp
$Comp
L arduradio-rescue:GND-power #PWR06
U 1 1 6047B8BA
P 6500 4520
F 0 "#PWR06" H 6500 4270 50  0001 C CNN
F 1 "GND" H 6505 4347 50  0000 C CNN
F 2 "" H 6500 4520 50  0001 C CNN
F 3 "" H 6500 4520 50  0001 C CNN
	1    6500 4520
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6500 4520 6440 4520
Wire Wire Line
	6140 4520 6060 4520
Wire Wire Line
	5730 4190 6830 4190
Wire Wire Line
	6830 4190 6830 4620
Wire Wire Line
	5160 4520 5110 4520
Wire Wire Line
	5110 4190 5430 4190
Wire Wire Line
	5110 4190 5110 4520
$Comp
L arduradio-rescue:R-Device R?
U 1 1 6048BF15
P 4890 4190
AR Path="/5FD94276/6048BF15" Ref="R?"  Part="1" 
AR Path="/6048BF15" Ref="R1"  Part="1" 
F 0 "R1" V 4980 4250 50  0000 L CNN
F 1 "100k" V 4980 4030 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4820 4190 50  0001 C CNN
F 3 "~" H 4890 4190 50  0001 C CNN
	1    4890 4190
	0    1    -1   0   
$EndComp
Wire Wire Line
	5040 4190 5110 4190
Connection ~ 5110 4190
Wire Wire Line
	4740 4190 3840 4190
Connection ~ 3840 4190
Wire Wire Line
	3840 4190 3840 4720
Wire Wire Line
	4880 4620 5160 4620
Text Label 4580 3060 0    50   ~ 0
BIAS_TEE_CONTROL
Wire Wire Line
	4580 3060 4510 3060
Text Label 4590 2920 0    50   ~ 0
AMP_Mute
Wire Wire Line
	4510 2920 4590 2920
$Comp
L arduradio:PESD5V0X1BT D?
U 1 1 602F6EB0
P 7900 4310
AR Path="/5FD932E8/602F6EB0" Ref="D?"  Part="1" 
AR Path="/602F6EB0" Ref="D2"  Part="1" 
F 0 "D2" V 8380 4620 50  0000 R CNN
F 1 "PESD5V0X1BT" V 8680 5030 50  0001 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8490 4260 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/PESD5V0X1BQ_PESD5V0X1BT.pdf" H 7900 4310 50  0001 C CNN
	1    7900 4310
	0    1    1    0   
$EndComp
Wire Wire Line
	6480 2790 6480 3010
Wire Wire Line
	6480 3010 6610 3010
Wire Wire Line
	6610 3010 6610 3040
Connection ~ 6480 2790
Wire Wire Line
	6610 3640 6610 3770
Wire Wire Line
	6610 3770 6840 3770
Wire Wire Line
	6840 3770 6840 3060
Connection ~ 6840 3060
Wire Wire Line
	6840 3060 6750 3060
Wire Wire Line
	8000 4660 8000 4620
Connection ~ 8000 4620
Wire Wire Line
	8000 4620 8210 4620
Wire Wire Line
	8000 5260 8000 5380
Wire Wire Line
	8000 5380 8480 5380
Wire Wire Line
	8480 5380 8480 4850
Connection ~ 8480 4850
$Comp
L arduradio:PESD5V0X1BT D?
U 1 1 602F6EB7
P 6510 2690
AR Path="/5FD932E8/602F6EB7" Ref="D?"  Part="1" 
AR Path="/602F6EB7" Ref="D1"  Part="1" 
F 0 "D1" V 7030 2710 50  0000 R CNN
F 1 "PESD5V0X1BT" V 7290 2710 50  0001 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7100 2640 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/PESD5V0X1BQ_PESD5V0X1BT.pdf" H 6510 2690 50  0001 C CNN
	1    6510 2690
	0    1    1    0   
$EndComp
NoConn ~ 6790 3340
NoConn ~ 8180 4960
$Sheet
S 5500 2070 690  960 
U 5FD932E8
F0 "SI4735_D60" 50
F1 "SI4735_D60.sch" 50
F2 "RST" I L 5500 2620 50 
F3 "SDIO" I L 5500 2320 50 
F4 "SCLK" I L 5500 2470 50 
F5 "ROUT" O R 6190 2330 50 
F6 "LOUT" O R 6190 2180 50 
F7 "Vin" I L 5500 2170 50 
F8 "FM_Ant" I R 6190 2470 50 
F9 "AM_Ant" I R 6190 2610 50 
F10 "SI4735_INT" O L 5500 2770 50 
F11 "SI4735_GPO1" O L 5500 2930 50 
$EndSheet
Wire Wire Line
	7190 1110 6390 1110
Wire Wire Line
	7190 1110 7190 1405
Text Label 7290 2630 2    50   ~ 0
Shdn
Text Label 6510 1210 0    50   ~ 0
Shdn
Wire Wire Line
	6390 1210 6510 1210
Wire Wire Line
	6390 4090 8210 4090
Wire Wire Line
	7190 1405 5020 1405
Wire Wire Line
	2750 4190 3840 4190
$Comp
L arduradio-rescue:R-Device R?
U 1 1 61169B02
P 5020 1555
AR Path="/5FD94276/61169B02" Ref="R?"  Part="1" 
AR Path="/61169B02" Ref="R2"  Part="1" 
F 0 "R2" V 5110 1615 50  0000 L CNN
F 1 "100k" V 5110 1395 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4950 1555 50  0001 C CNN
F 3 "~" H 5020 1555 50  0001 C CNN
	1    5020 1555
	1    0    0    1   
$EndComp
Connection ~ 5020 1405
Wire Wire Line
	5020 1405 2750 1405
$Comp
L arduradio-rescue:GND-power #PWR03
U 1 1 6116A2AC
P 5020 2100
F 0 "#PWR03" H 5020 1850 50  0001 C CNN
F 1 "GND" H 5025 1927 50  0000 C CNN
F 2 "" H 5020 2100 50  0001 C CNN
F 3 "" H 5020 2100 50  0001 C CNN
	1    5020 2100
	1    0    0    -1  
$EndComp
Connection ~ 7190 1405
Wire Wire Line
	7190 1405 7190 2030
Wire Wire Line
	2750 1405 2750 4190
$Comp
L arduradio-rescue:R-Device R?
U 1 1 61168B7F
P 5020 1905
AR Path="/5FD94276/61168B7F" Ref="R?"  Part="1" 
AR Path="/61168B7F" Ref="R3"  Part="1" 
F 0 "R3" V 5110 1965 50  0000 L CNN
F 1 "100k" V 5110 1745 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4950 1905 50  0001 C CNN
F 3 "~" H 5020 1905 50  0001 C CNN
	1    5020 1905
	1    0    0    1   
$EndComp
Wire Wire Line
	5020 2100 5020 2055
Wire Wire Line
	5020 1705 5020 1730
Wire Wire Line
	5020 1730 4680 1730
Wire Wire Line
	4680 1730 4680 2165
Wire Wire Line
	4680 2165 4510 2165
Connection ~ 5020 1730
Wire Wire Line
	5020 1730 5020 1755
$EndSCHEMATC
