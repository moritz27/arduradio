EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4240 3100 1270 190 
U 60362EF5
F0 "Lithium_Bat_Management" 50
F1 "Lithium_Bat_Management.sch" 50
F2 "Out+" O R 5510 3190 50 
$EndSheet
$Comp
L arduradio-rescue:VCC-power #PWR058
U 1 1 6034888A
P 7700 3190
F 0 "#PWR058" H 7700 3040 50  0001 C CNN
F 1 "VCC" H 7715 3363 50  0000 C CNN
F 2 "" H 7700 3190 50  0001 C CNN
F 3 "" H 7700 3190 50  0001 C CNN
	1    7700 3190
	1    0    0    -1  
$EndComp
Wire Wire Line
	7480 3250 7700 3250
Wire Wire Line
	7700 3250 7700 3190
Wire Wire Line
	5510 3190 5650 3190
$Sheet
S 6610 3090 870  360 
U 603908EF
F0 "linear_reg" 50
F1 "linear_reg.sch" 50
F2 "Vin" I L 6610 3190 50 
F3 "Vout" O R 7480 3250 50 
F4 "OFF" I L 6610 3330 50 
$EndSheet
Text HLabel 6010 4400 2    50   Output ~ 0
Vbat
Connection ~ 5650 3190
$Comp
L Switch:SW_SPDT SW11
U 1 1 60C1C434
P 6200 3460
F 0 "SW11" H 6200 3227 50  0000 C CNN
F 1 "SW_SPDT" H 6200 3654 50  0001 C CNN
F 2 "Button_Switch_THT:SW_CuK_OS102011MA1QN1_SPDT_Angled" H 6200 3460 50  0001 C CNN
F 3 "~" H 6200 3460 50  0001 C CNN
	1    6200 3460
	-1   0    0    1   
$EndComp
Wire Wire Line
	5650 4400 6010 4400
Wire Wire Line
	5650 3190 6610 3190
Wire Wire Line
	6400 3460 6510 3460
Wire Wire Line
	6510 3460 6510 3330
Wire Wire Line
	6510 3330 6610 3330
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60C290E3
P 5920 3810
AR Path="/5FD94276/60C290E3" Ref="R?"  Part="1" 
AR Path="/60C290E3" Ref="R?"  Part="1" 
AR Path="/60457E44/60C290E3" Ref="R40"  Part="1" 
AR Path="/60457E44/603908EF/60C290E3" Ref="R?"  Part="1" 
F 0 "R40" H 6000 3780 50  0000 L CNN
F 1 "10k" H 6000 3860 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5850 3810 50  0001 C CNN
F 3 "~" H 5920 3810 50  0001 C CNN
	1    5920 3810
	1    0    0    1   
$EndComp
Wire Wire Line
	5920 3660 5920 3560
Wire Wire Line
	5920 3560 6000 3560
$Comp
L arduradio-rescue:GND-power #PWR057
U 1 1 60C29A5B
P 5920 4030
AR Path="/60457E44/60C29A5B" Ref="#PWR057"  Part="1" 
AR Path="/60457E44/603908EF/60C29A5B" Ref="#PWR?"  Part="1" 
AR Path="/60C29A5B" Ref="#PWR?"  Part="1" 
F 0 "#PWR057" H 5920 3780 50  0001 C CNN
F 1 "GND" H 5925 3857 50  0000 C CNN
F 2 "" H 5920 4030 50  0001 C CNN
F 3 "" H 5920 4030 50  0001 C CNN
	1    5920 4030
	1    0    0    -1  
$EndComp
Wire Wire Line
	5920 4030 5920 3960
Text HLabel 7720 3740 2    50   Output ~ 0
SHDN
Wire Wire Line
	6510 3460 6510 3740
Wire Wire Line
	6510 3740 7720 3740
Connection ~ 6510 3460
Wire Wire Line
	5650 3190 5650 3360
$Comp
L arduradio-rescue:R-Device R?
U 1 1 60C2A952
P 5820 3360
AR Path="/5FD94276/60C2A952" Ref="R?"  Part="1" 
AR Path="/60C2A952" Ref="R?"  Part="1" 
AR Path="/60457E44/60C2A952" Ref="R39"  Part="1" 
AR Path="/60457E44/603908EF/60C2A952" Ref="R?"  Part="1" 
F 0 "R39" V 5910 3230 50  0000 L CNN
F 1 "10k" V 5910 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5750 3360 50  0001 C CNN
F 3 "~" H 5820 3360 50  0001 C CNN
	1    5820 3360
	0    -1   1    0   
$EndComp
Wire Wire Line
	5670 3360 5650 3360
Connection ~ 5650 3360
Wire Wire Line
	5650 3360 5650 4400
Wire Wire Line
	5970 3360 6000 3360
$EndSCHEMATC
