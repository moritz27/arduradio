#include <SI4735.h>

#include "SPI.h"
//#include "Adafruit_GFX.h"
//#include "Adafruit_ILI9341.h"

#include "SPI.h"
#include "TFT_eSPI.h"

#include "Rotary.h"

// Test it with patch_init.h or patch_full.h. Do not try load both.
#include "patch_init.h" // SSB patch for whole SSBRX initialization string

const uint16_t size_content = sizeof ssb_patch_content; // see ssb_patch_content in patch_full.h or patch_init.h

#define MUTE_AMP PB13

#define TFT_DC PA9
#define TFT_RST PA8
#define TFT_CS PA4
#define TFT_CLK PA5
#define TFT_MISO PA6
#define TFT_MOSI PA7
#define TFT_LED 0  // 0 if wired to +3.3V directly

#define SD_CS PA3

#define FM_BAND_TYPE 0
#define MW_BAND_TYPE 1
#define SW_BAND_TYPE 2
#define LW_BAND_TYPE 3

#define RESET_PIN PB8
#define SEN_PIN PB9

// Enconder PINs
#define ENCODER_PIN_A PB10
#define ENCODER_PIN_B PB11

// Poti PIN
#define POTI_PIN PA0

// Buttons controllers
//#define MODE_SWITCH  PA1     // Switch MODE (Am/LSB/USB)
//#define BANDWIDTH_BUTTON PB5 // Used to select the banddwith. Values: 1.2, 2.2, 3.0, 4.0, 0.5, 1.0 KHz
//#define BAND_BUTTON_UP PA3   // Next band
//#define BAND_BUTTON_DOWN PA2 // Previous band
//#define AGC_SWITCH  PB12     //  Switch AGC ON/OF
//#define STEP_SWITCH PC13     //  Used to select the increment or decrement frequency step (1, 5 or 10 KHz)
//#define BFO_SWITCH  PB4     // Used to select the enconder control (BFO or VFO)


#define LED_1 PB3
uint8_t led_1_value = 0;
uint8_t led_1_upwards = 1;

#define LED_2 PB4
uint8_t led_2_value = 0;
uint8_t led_2_upwards = 1;

#define MODE_SWITCH  PA1     // Switch MODE (Am/LSB/USB)
#define BANDWIDTH_BUTTON PA2 // Used to select the banddwith. Values: 1.2, 2.2, 3.0, 4.0, 0.5, 1.0 KHz
#define BAND_BUTTON_UP PB0   // Next band
#define BAND_BUTTON_DOWN PB1 // Previous band
#define AGC_SWITCH  PC13     //  Switch AGC ON/OF
#define STEP_SWITCH PB9     //  Used to select the increment or decrement frequency step (1, 5 or 10 KHz)
#define BFO_SWITCH PB15      // Used to select the enconder control (BFO or VFO)
#define BIAS_SWITCH PA15      // Used to activate the bias voltage
#define ROTARY_SWITCH  PB12     //  Switch AGC ON/OFF

#define MIN_ELAPSED_TIME 100
#define MIN_ELAPSED_RSSI_TIME 150
#define DEFAULT_VOLUME 0 // change it for your favorite sound volume
#define VOLUME_INPUT_HYSTERSIS 20

#define FM 0
#define LSB 1
#define USB 2
#define AM 3
#define LW 4

#define SSB 1

#define CLEAR_BUFFER(x)  (x[0] = '\0');

bool bfoOn = false;
bool disableAgc = true;
bool ssbLoaded = false;
bool fmStereo = true;

int currentBFO = 0;

long elapsedRSSI = millis();
long elapsedButton = millis();

// Encoder control variables
volatile int encoderCount = 0;

// Some variables to check the SI4735 status
uint16_t currentFrequency;

uint8_t currentBFOStep = 25;

uint8_t bwIdxSSB = 2;
const char * bandwitdthSSB[] = {"1.2", "2.2", "3.0", "4.0", "0.5", "1.0"};

uint8_t bwIdxAM = 1;
const char * bandwitdthAM[] = {"6", "4", "3", "2", "1", "1.8", "2.5"};

const char * bandModeDesc[] = {"FM ", "LSB", "USB", "AM "};
uint8_t currentMode = FM;
uint8_t lastMode = AM;

uint16_t currentStep = 1;


char bufferDisplay[40]; // Useful to handle string
char bufferFreq[15];
char bufferBFO[15];
char bufferStepVFO[15];
char bufferStepBFO[15];
char bufferBW[15];
char bufferAGC[15];
char bufferBand[15];
char bufferStereo[15];
char bufferRSSI[15];
char bufferSNR[15];
char bufferVolume[4];

/*
   Band data structure
*/
typedef struct
{
  const char *bandName; // Band description
  uint8_t bandType;     // Band type (FM, MW or SW)
  uint16_t minimumFreq; // Minimum frequency of the band
  uint16_t maximumFreq; // maximum frequency of the band
  uint16_t currentFreq; // Default frequency or current frequency
  uint16_t currentStep; // Defeult step (increment and decrement)
} Band;

/*
   Band table
*/
Band band[] = {
  {"FM ", FM_BAND_TYPE, 6400, 10800, 10380, 10},
  {"LW ", LW_BAND_TYPE, 100, 510, 300, 1},
  {"AM ", MW_BAND_TYPE, 520, 1720, 810, 10},
  {"80m", SW_BAND_TYPE, 3000, 4500, 3700, 1}, // 80 meters - 160 meters
  {"60m", SW_BAND_TYPE, 4500, 6300, 6000, 5}, //
  {"40m", SW_BAND_TYPE, 6800, 7800, 7100, 5}, // 40 meters
  {"31m", SW_BAND_TYPE, 9200, 10000, 9600, 5},
  {"25m", SW_BAND_TYPE, 11200, 12500, 11940, 5},
  {"22m", SW_BAND_TYPE, 13400, 13900, 13600, 5},
  {"20m", SW_BAND_TYPE, 14000, 14500, 14200, 1}, // 20 meters
  {"19m", SW_BAND_TYPE, 15000, 15900, 15300, 5},
  {"17m", SW_BAND_TYPE, 18000, 18300, 18100, 1}, // 17 meters
  {"15m", SW_BAND_TYPE, 21000, 21900, 21200, 1}, // 15 mters
  {"CB ", SW_BAND_TYPE, 26200, 27900, 27500, 1}, // CB band (11 meters)
  {"10m", SW_BAND_TYPE, 28000, 30000, 28400, 1},
  {"All", SW_BAND_TYPE, 100, 30000, 14200, 1}, // ALL SW (From 1.7 to 30 MHZ)
};

const int lastBand = (sizeof band / sizeof(Band)) - 1;
int bandIdx = 0;

uint8_t rssi = 0;
uint8_t snr = 0;
uint8_t stereo = 1;
//uint8_t volume = DEFAULT_VOLUME;
uint16_t last_volume = 1025;

// Devices class declarations
Rotary encoder = Rotary(ENCODER_PIN_A, ENCODER_PIN_B);
//Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_CLK, TFT_RST, TFT_MISO);
//Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);
TFT_eSPI tft = TFT_eSPI();      
SI4735 si4735;

void setup()
{

  // LED Pins
  pinMode(LED_1, OUTPUT);
  digitalWrite(LED_1, HIGH);
  pinMode(LED_2, OUTPUT);
  digitalWrite(LED_2, HIGH);
  
  // Encoder pins
  pinMode(ENCODER_PIN_A, INPUT_PULLUP);
  pinMode(ENCODER_PIN_B, INPUT_PULLUP);
  pinMode(BANDWIDTH_BUTTON, INPUT_PULLUP);
  pinMode(BAND_BUTTON_UP, INPUT_PULLUP);
  pinMode(BAND_BUTTON_DOWN, INPUT_PULLUP);
  pinMode(BFO_SWITCH, INPUT_PULLUP);
  pinMode(AGC_SWITCH, INPUT_PULLUP);
  pinMode(STEP_SWITCH, INPUT_PULLUP);
  pinMode(MODE_SWITCH, INPUT_PULLUP);

  pinMode(POTI_PIN, INPUT);

  pinMode(SD_CS, OUTPUT);
  digitalWrite(SD_CS, HIGH);

  pinMode(MUTE_AMP, OUTPUT);
  digitalWrite(MUTE_AMP, LOW);
  

  // Use this initializer if using a 1.8" TFT screen:
  //delay(5000);
  tft.begin();
  //delay(1000);
  tft.setRotation(3);
  tft.fillScreen(ILI9341_BLACK);
  //delay(1000);
  showTemplate();

  // Encoder interrupt
  attachInterrupt(digitalPinToInterrupt(ENCODER_PIN_A), rotaryEncoder, CHANGE);
  attachInterrupt(digitalPinToInterrupt(ENCODER_PIN_B), rotaryEncoder, CHANGE);

  si4735.setup(RESET_PIN, 1);

  // Set up the radio for the current band (see index table variable bandIdx )
  useBand();
  si4735.setVolume(DEFAULT_VOLUME);
  digitalWrite(MUTE_AMP, HIGH);
  showStatus();
  digitalWrite(LED_1, LOW);
  digitalWrite(LED_2, LOW);
}

/*
    Reads encoder via interrupt
    Use Rotary.h and  Rotary.cpp implementation to process encoder via interrupt
*/
void rotaryEncoder()
{ // rotary encoder events
  uint8_t encoderStatus = encoder.process();
  if (encoderStatus)
    encoderCount = (encoderStatus == DIR_CW) ? 1 : -1;
}

void loop()
{
  fade_led_2();
  checkInputs();
  checkRSSI();
  delay(10);
}
