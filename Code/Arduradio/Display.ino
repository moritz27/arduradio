const char * const text = "PU2CLR-SI4735";

#define FREQ_X 15
#define FREQ_Y 15
#define FREQ_FONT_SIZE 3
#define FREQ_UNIT_X FREQ_X + 135
#define FREQ_UNIT_Y FREQ_Y + 10
#define FREQ_UNIT_FONT_SIZE 2


#define OUTPUT_VOLUME_NAME_X 205
#define OUTPUT_VOLUME_NAME_Y FREQ_Y + 10
#define OUTPUT_VOLUME_NAME_FONT_SIZE 2
#define OUTPUT_VOLUME_X OUTPUT_VOLUME_NAME_X + 55
#define OUTPUT_VOLUME_Y FREQ_Y
#define OUTPUT_VOLUME_FONT_SIZE 3


// Second Line

#define BAND_X 10
#define BAND_Y 60
#define BAND_FONT_SIZE 2

#define AGC_X BAND_X+100
#define AGC_Y BAND_Y
#define AGC_FONT_SIZE 2

#define PILOT_X AGC_X + 95
#define PILOT_Y BAND_Y
#define PILOT_FONT_SIZE 2

#define BW_X PILOT_X - 25
#define BW_Y PILOT_Y
#define BW_FONT_SIZE 2


#define BFO_NAME_X 10
#define BFO_NAME_Y 130
//#define BFO_NAME_X 184// PILOT_X //154
//#define BFO_NAME_Y 150 // PILOT_Y //100
#define BFO_NAME_FONT_SIZE 1
#define BFO_X BFO_NAME_X + 30
#define BFO_Y BFO_NAME_Y
#define BFO_FONT_SIZE 1

#define BFO_STEP_NAME_X BFO_NAME_X
#define BFO_STEP_NAME_Y BFO_NAME_Y + 10
#define BFO_STEP_NAME_FONT_SIZE 1
#define BFO_STEP_X BFO_X
#define BFO_STEP_Y BFO_STEP_NAME_Y
#define BFO_STEP_FONT_SIZE 1




// Third Line

#define SNR_NAME_X 10
#define SNR_NAME_Y 95
#define SNR_NAME_FONT_SIZE 2
#define SNR_X SNR_NAME_X + 60
#define SNR_Y SNR_NAME_Y
#define SNR_FONT_SIZE 2

//#define RSSI_NAME_X SNR_X
//#define RSSI_NAME_Y SNR_NAME_Y + 50
//#define RSSI_NAME_FONT_SIZE 2
//#define RSSI_X RSSI_NAME_X + 60
//#define RSSI_Y RSSI_NAME_Y
//#define RSSI_FONT_SIZE 2

#define RSSI_NAME_X SNR_X + 45
#define RSSI_NAME_Y SNR_NAME_Y
#define RSSI_NAME_FONT_SIZE 2
#define RSSI_X RSSI_NAME_X + 60
#define RSSI_Y RSSI_NAME_Y
#define RSSI_FONT_SIZE 2

#define VFO_STEP_X RSSI_X + 48
#define VFO_STEP_Y SNR_NAME_Y
#define VFO_STEP_FONT_SIZE 2

#define FONT_DEFAULT_SPACE 10
#define FONT_1_SPACE 6
#define FONT_2_SPACE 12
#define FONT_3_SPACE 18




void printValue(int col, int line, char *oldValue, char *newValue, uint16_t color, uint8_t font_size) {
  int c = col;
  char * pOld;
  char * pNew;

  uint8_t space;

  switch (font_size) {
    case 1: space = FONT_1_SPACE;
      break;
    case 2: space = FONT_2_SPACE;
      break;
    case 3: space = FONT_3_SPACE;
      break;
    default: space = FONT_DEFAULT_SPACE;
  }

  pOld = oldValue;
  pNew = newValue;

  // prints just changed digits
  while (*pOld && *pNew)
  {
    if (*pOld != *pNew)
    {
      tft.drawChar(c, line, *pOld, ILI9341_BLACK, ILI9341_BLACK, font_size);
      tft.drawChar(c, line, *pNew, color, ILI9341_BLACK, font_size);
    }
    pOld++;
    pNew++;
    c += space;
  }

  // Is there anything else to erase?
  while (*pOld)
  {
    tft.drawChar(c, line, *pOld, ILI9341_BLACK, ILI9341_BLACK, font_size);
    pOld++;
    c += space;
  }

  // Is there anything else to print?
  while (*pNew)
  {
    tft.drawChar(c, line, *pNew, color, ILI9341_BLACK, font_size);
    pNew++;
    c += space;
  }

  // Save the current content to be tested next time
  strcpy(oldValue, newValue);
}


/*
   Shows the static content on  display
*/
void showTemplate()
{
  int maxY1 = tft.height() - 1;
  int maxX1 = tft.width() - 1;

  tft.drawRect(0, 0, maxX1, maxY1, ILI9341_WHITE);
  tft.drawRect(2, 2, maxX1 - 4, 50, ILI9341_YELLOW);

  //tft.drawLine(FREQ_UNIT_X + 70, 2, FREQ_UNIT_X + 70, 50, ILI9341_YELLOW);  //Freq Block
  tft.drawRect(FREQ_UNIT_X + 48, 2, maxX1 - 4, 50, ILI9341_YELLOW);



  tft.drawLine(0, 80, maxX1 - 2, 80, ILI9341_YELLOW); // second horiz. line

  tft.drawLine(BAND_X + 85, 50, BAND_X + 85, 80, ILI9341_YELLOW); // Mode Block
  tft.drawLine(AGC_X + 50, 50, AGC_X + 50, 80, ILI9341_YELLOW);  // Band name

  tft.drawLine(2, 120, maxX1 - 2, 120, ILI9341_YELLOW); // third horiz. line

  tft.drawLine(SNR_X + 38, 80, SNR_X + 38, 120, ILI9341_YELLOW); // SNR Block
  tft.drawLine(RSSI_X + 38, 80, RSSI_X + 38, 120, ILI9341_YELLOW);  // RSSI name

  tft.setTextColor(ILI9341_LIGHTGREY);

  tft.setTextSize(SNR_NAME_FONT_SIZE);
  tft.setCursor(SNR_NAME_X, SNR_NAME_Y);
  tft.println("SNR:");
  tft.setTextSize(RSSI_NAME_FONT_SIZE);
  tft.setCursor(RSSI_NAME_X, RSSI_NAME_Y);
  tft.println("RSSI:");
  tft.setTextSize(OUTPUT_VOLUME_NAME_FONT_SIZE);
  tft.setCursor(OUTPUT_VOLUME_NAME_X, OUTPUT_VOLUME_NAME_Y);
  tft.println("VOL:");

  //tft.setTextColor(ILI9341_YELLOW);
  //tft.setTextSize(1);

  //tft.setCursor(5, 90);
  //tft.println(text_arduino_library);

  //tft.setCursor(5, 110);
  //tft.println(text_example);

  //tft.setCursor(5, 130);
  //tft.println(text_message);
}

/*
   Shows frequency information on Display
*/
void showFrequency()
{
  float freq;
  uint16_t color;

  if (si4735.isCurrentTuneFM())
  {
    freq = currentFrequency / 100.0;
    dtostrf(freq, 3, 1, bufferDisplay);
  }
  else
  {
    freq = currentFrequency / 1000.0;
    if (currentFrequency < 1000)
      sprintf(bufferDisplay, "%3d", currentFrequency);
    else
      dtostrf(freq, 2, 3, bufferDisplay);
  }
  //Serial.println(currentFrequency);
  color = (bfoOn) ? ILI9341_CYAN : ILI9341_YELLOW;
  printValue(FREQ_X, FREQ_Y, bufferFreq, bufferDisplay, color, FREQ_FONT_SIZE);
}


/*
    Show some basic information on display
*/
void showStatus()
{
  si4735.getStatus();
  si4735.getCurrentReceivedSignalQuality();
  // SRN
  currentFrequency = si4735.getFrequency();
  showFrequency();

  if (currentMode != lastMode) {
    lastMode = currentMode;
    if (si4735.isCurrentTuneFM())
    {
      tft.setTextColor(ILI9341_BLACK);
      tft.setTextSize(FREQ_UNIT_FONT_SIZE);
      tft.setCursor(FREQ_UNIT_X, FREQ_UNIT_Y);
      tft.println("KHz");
      tft.setTextColor(ILI9341_RED);
      tft.setCursor(FREQ_UNIT_X, FREQ_UNIT_Y);
      tft.println("MHz");

      printValue(VFO_STEP_X, VFO_STEP_Y, bufferStepVFO, bufferDisplay, ILI9341_BLACK, VFO_STEP_FONT_SIZE);
      showBFOTemplate(ILI9341_BLACK);
      tft.setTextColor(ILI9341_BLACK);
      tft.setTextSize(BW_FONT_SIZE);
      tft.setCursor(BW_X, BW_Y);
      tft.println(bufferBW);
      CLEAR_BUFFER(bufferBW);
    }
    else
    {
      tft.setTextColor(ILI9341_BLACK);
      tft.setTextSize(FREQ_UNIT_FONT_SIZE);
      tft.setCursor(FREQ_UNIT_X, FREQ_UNIT_Y);
      tft.println("MHz");
      tft.setTextColor(ILI9341_RED);
      tft.setCursor(FREQ_UNIT_X, FREQ_UNIT_Y);
      tft.println("KHz");
      
      tft.setTextColor(ILI9341_BLACK);
      tft.setTextSize(PILOT_FONT_SIZE);
      tft.setCursor(PILOT_X, PILOT_Y);
      tft.println(bufferStereo);
    }
  }

  if (!si4735.isCurrentTuneFM()) {
    sprintf(bufferDisplay, "Stp: %2.2d", currentStep);
    printValue(VFO_STEP_X, VFO_STEP_Y, bufferStepVFO, bufferDisplay, ILI9341_YELLOW, VFO_STEP_FONT_SIZE);
  }

  if (band[bandIdx].bandType == SW_BAND_TYPE)
    sprintf(bufferDisplay, "%s %s", band[bandIdx].bandName, bandModeDesc[currentMode]);
  else
    sprintf(bufferDisplay, "%s", band[bandIdx].bandName);
  printValue(BAND_X, BAND_Y, bufferBand, bufferDisplay, ILI9341_CYAN, BAND_FONT_SIZE);

  // AGC
  si4735.getAutomaticGainControl();
  //sprintf(bufferDisplay, "AGC %s", (si4735.isAgcEnabled()) ? "ON  " : "OFF");
  tft.setTextColor((si4735.isAgcEnabled()) ? ILI9341_GREEN : ILI9341_RED);
  tft.setTextSize(AGC_FONT_SIZE);
  tft.setCursor(AGC_X, AGC_Y);
  tft.println("AGC");
  //printValue(AGC_X, AGC_Y, bufferAGC, bufferDisplay, ILI9341_CYAN, AGC_FONT_SIZE);
  showFilter();
}


void showFilter() {

  // Bandwidth
  if (currentMode == LSB || currentMode == USB || currentMode == AM) {
    char * bw;

//    tft.setTextColor(ILI9341_BLACK);
//    tft.setTextSize(PILOT_FONT_SIZE);
//    tft.setCursor(PILOT_X, PILOT_Y);
//    tft.println(bufferStereo);

    if (currentMode == AM) {
      bw = (char *) bandwitdthAM[bwIdxAM];
    }
    else {
      bw = (char *) bandwitdthSSB[bwIdxSSB];
      showBFOTemplate(ILI9341_CYAN);
      showBFO();
    }
    sprintf(bufferDisplay, "BW: %s KHz", bw);
    printValue(BW_X, BW_Y, bufferBW, bufferDisplay, ILI9341_CYAN, BW_FONT_SIZE);
  }

}

/* *******************************
   Shows RSSI status
*/
void showRSSI()
{
  int rssiLevel;
  int snrLevel;
  int maxAux = tft.width();

  if (currentMode == FM)
  {
    sprintf(bufferDisplay, "%s", (si4735.getCurrentPilot()) ? "STEREO" : "MONO");
    printValue(PILOT_X, PILOT_Y, bufferStereo, bufferDisplay, ILI9341_CYAN, PILOT_FONT_SIZE);
  }

  // SNR.: 0 to 127 dB
  snrLevel = 47 + map(snr, 0, 127, 0, ( maxAux  - 43) );
  itoa(snrLevel, bufferDisplay, 10);
  printValue(SNR_X, SNR_Y, bufferSNR, bufferDisplay, ILI9341_CYAN, SNR_FONT_SIZE);

  // RSSI: 0 to 127 dBuV
  rssiLevel = 47 + map(rssi, 0, 127, 0, ( maxAux  - 43) );
  itoa(rssiLevel, bufferDisplay, 10);
  printValue(RSSI_X, RSSI_Y, bufferRSSI, bufferDisplay, ILI9341_CYAN, RSSI_FONT_SIZE);
}

void showBFOTemplate(uint16_t color)
{
  tft.setTextColor(ILI9341_BLACK);
  tft.setTextSize(PILOT_FONT_SIZE);
  tft.setCursor(PILOT_X, PILOT_Y);
  //tft.println(bufferStereo);

  tft.setTextColor(color);
  tft.setTextSize(BFO_NAME_FONT_SIZE);
  tft.setCursor(BFO_NAME_X, BFO_NAME_Y);
  tft.println("BFO.:");
  tft.setTextSize(BFO_STEP_NAME_FONT_SIZE);
  tft.setCursor(BFO_STEP_NAME_X, BFO_STEP_NAME_Y);
  tft.println("Step:");
}

void clearBFO() {
  //tft.fillRect(124, 52, 218, 73, ILI9341_BLACK); // Clear All BFO area
  CLEAR_BUFFER(bufferBFO);
  CLEAR_BUFFER(bufferStepBFO);
}

void showBFO()
{
  sprintf(bufferDisplay, "%+4d", currentBFO);
  printValue(BFO_X, BFO_Y, bufferBFO, bufferDisplay, ILI9341_CYAN, BFO_FONT_SIZE);

  sprintf(bufferDisplay, "%4d", currentBFOStep);
  printValue(BFO_STEP_X, BFO_STEP_Y, bufferStepBFO, bufferDisplay, ILI9341_CYAN, BFO_STEP_FONT_SIZE);
}

void showVolume(uint8_t display_volume) {
  sprintf(bufferDisplay, "%i", display_volume);
  printValue(OUTPUT_VOLUME_X, OUTPUT_VOLUME_Y, bufferVolume, bufferDisplay, ILI9341_CYAN, OUTPUT_VOLUME_FONT_SIZE);
}
