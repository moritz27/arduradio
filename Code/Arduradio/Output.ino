void fade_led_1(void){
   analogWrite(LED_1, led_1_value);
  if (led_1_upwards == 1){
    //Serial.print("counting up.. ");
    led_1_value++;
  }
  else{
    //Serial.print("counting down.. ");
    led_1_value--;
  }
  if(led_1_value >= 254){
    led_1_upwards = 0;
    //led_1_value = 255;
  }
  else if(led_1_value < 1){
    led_1_upwards = 1;
    //led_1_value = 0;
  }
  //Serial.println(led_1_value);
}

void fade_led_2(void){
   analogWrite(LED_2, led_2_value);
  if (led_2_upwards == 2){
    //Serial.print("counting up.. ");
    led_2_value++;
  }
  else{
    //Serial.print("counting down.. ");
    led_2_value--;
  }
  if(led_2_value >= 254){
    led_2_upwards = 0;
    //led_2_value = 255;
  }
  else if(led_2_value < 1){
    led_2_upwards = 1;
    //led_1_value = 0;
  }
  //Serial.println(led_1_value);
}
