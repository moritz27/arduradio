/*
   Goes to the next band (see Band table)
*/
void bandUp()
{
  digitalWrite(MUTE_AMP, LOW);
  // save the current frequency for the band
  band[bandIdx].currentFreq = currentFrequency;
  band[bandIdx].currentStep = currentStep;
  bandIdx = (bandIdx < lastBand) ? (bandIdx + 1) : 0;
  useBand();
  digitalWrite(MUTE_AMP, HIGH);
}

/*
   Goes to the previous band (see Band table)
*/
void bandDown()
{
  digitalWrite(MUTE_AMP, LOW);
  // save the current frequency for the band
  band[bandIdx].currentFreq = currentFrequency;
  band[bandIdx].currentStep = currentStep;
  bandIdx = (bandIdx > 0) ? (bandIdx - 1) : lastBand;
  useBand();
  digitalWrite(MUTE_AMP, HIGH);
}

/*
   This function loads the contents of the ssb_patch_content array into the CI (Si4735) and starts the radio on
   SSB mode.
*/
void loadSSB()
{
  digitalWrite(MUTE_AMP, LOW);
  si4735.reset();
  si4735.queryLibraryId(); // Is it really necessary here? I will check it.
  si4735.patchPowerUp();
  delay(50);
  si4735.setI2CFastMode(); // Recommended
  // si4735.setI2CFastModeCustom(500000); //  It is a test and may crash.
  si4735.downloadPatch(ssb_patch_content, size_content);
  si4735.setI2CStandardMode(); // goes back to default (100KHz)

  // Parameters
  // AUDIOBW - SSB Audio bandwidth; 0 = 1.2KHz (default); 1=2.2KHz; 2=3KHz; 3=4KHz; 4=500Hz; 5=1KHz;
  // SBCUTFLT SSB - side band cutoff filter for band passand low pass filter ( 0 or 1)
  // AVC_DIVIDER  - set 0 for SSB mode; set 3 for SYNC mode.
  // AVCEN - SSB Automatic Volume Control (AVC) enable; 0=disable; 1=enable (default).
  // SMUTESEL - SSB Soft-mute Based on RSSI or SNR (0 or 1).
  // DSP_AFCDIS - DSP AFC Disable or enable; 0=SYNC MODE, AFC enable; 1=SSB MODE, AFC disable.
  si4735.setSSBConfig(bwIdxSSB, 1, 0, 0, 0, 1);
  delay(25);
  ssbLoaded = true;
  digitalWrite(MUTE_AMP, HIGH);
}

/*
   Switch the radio to current band
*/
void useBand()
{
  showBFOTemplate(ILI9341_YELLOW);

  if (band[bandIdx].bandType == FM_BAND_TYPE)
  {
    currentMode = FM;
    si4735.setTuneFrequencyAntennaCapacitor(0);
    si4735.setFM(band[bandIdx].minimumFreq, band[bandIdx].maximumFreq, band[bandIdx].currentFreq, band[bandIdx].currentStep);
    bfoOn = ssbLoaded = false;
  }
  else
  {
    // set the tuning capacitor for SW or MW/LW
    // si4735.setTuneFrequencyAntennaCapacitor( (band[bandIdx].bandType == MW_BAND_TYPE || band[bandIdx].bandType == LW_BAND_TYPE) ? 0 : 1);
    si4735.setTuneFrequencyAntennaCapacitor(0);

    if (ssbLoaded)
    {
      si4735.setSSB(band[bandIdx].minimumFreq, band[bandIdx].maximumFreq, band[bandIdx].currentFreq, band[bandIdx].currentStep, currentMode);
      si4735.setSSBAutomaticVolumeControl(1);
      si4735.setSsbSoftMuteMaxAttenuation(0); // Disable Soft Mute for SSB
    }
    else
    {
      Serial.print("Tuning AM ");
      currentMode = AM;
      si4735.setAM(band[bandIdx].minimumFreq, band[bandIdx].maximumFreq, band[bandIdx].currentFreq, band[bandIdx].currentStep);
      si4735.setAM();
      delay(100);
      si4735.setSeekAmLimits(band[bandIdx].minimumFreq, band[bandIdx].maximumFreq);
      si4735.setFrequency(band[bandIdx].currentFreq);
      si4735.setFrequencyStep(band[bandIdx].currentStep);
      si4735.setAutomaticGainControl(1, 0);
      si4735.setAmSoftMuteMaxAttenuation(0); // // Disable Soft Mute for AM
      bfoOn = false;
    }
  }
  delay(100);
  
  Serial.print("Band ID: ");
  Serial.print(bandIdx);
  Serial.print(" Min Freq: ");
  Serial.print(band[bandIdx].minimumFreq);
  Serial.print(" Max Freq: ");
  Serial.print(band[bandIdx].maximumFreq);
  Serial.print(" Current Freq: ");
  Serial.print(band[bandIdx].currentFreq);
  Serial.print(" Current Step: ");
  Serial.print(band[bandIdx].currentStep);
  Serial.print(" Set Freq: ");
  Serial.println(si4735.getFrequency());

  currentFrequency = band[bandIdx].currentFreq;
  //Serial.println(currentFrequency);
  currentStep = band[bandIdx].currentStep;
  rssi = 0;
  clearBFO();
  //tft.fillRect(155, 3, 216, 20, ILI9341_BLACK);  // Clear Step field
  showStatus();
}
